@extends('layouts.app')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            {{$user->name}}
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <form action="{{url('logout')}}" method="post">
                        @csrf
                        <div class="col-sm-2 offset-sm-2">
                            <button type="submit" class="button">Logout</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
