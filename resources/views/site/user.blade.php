@extends('layouts.app')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="container">
                <div class="formBox">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <h1>Contact form</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="inputBox ">
                                    <div class="inputText">First Name</div>
                                    <input type="text" name="" class="input" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="inputBox">
                                    <div class="inputText">Last Name</div>
                                    <input type="text" name="" class="input" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="inputBox">
                                    <div class="inputText">Email</div>
                                    <input type="email" name="" class="input" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="inputBox">
                                    <div class="inputText">Phone Number</div>
                                    <input type="text" name="" class="input" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="inputBox">
                                    <div class="inputText">Age</div>
                                    <input type="text" name="" class="input" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div
                                    class="inputBox d-xl-flex d-lg-flex d-md-block d-sm-block pt-4 justify-content-between">
                                    <h2 class="form-text2">Gender</h2>
                                    <div class="custom-control custom-radio mt-2">
                                        <input type="radio" class="custom-control-input" id="defaultGroupExample1"
                                               name="groupOfDefaultRadios" checked="">
                                        <label class="custom-control-label" for="defaultGroupExample1">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio mt-2">
                                        <input type="radio" class="custom-control-input" id="defaultGroupExample2"
                                               name="groupOfDefaultRadios">
                                        <label class="custom-control-label" for="defaultGroupExample2">Female</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="submit" name="" class="button" value="Send Message">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
