@extends('layouts.app')

@section('content')
    <section>
        <div class="we-section-one p-0 m-0 p-5">
            <div class="we-section-one-inner pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h1>Краткая информация о нас</h1>
            </div>
        </div>
        <div class="we-section-two py-5">
            @foreach ($abouts as $key =>  $about)
                @if ($key%2 == 0)
                    <div class="row p-0 m-0">
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 py-5 px-0">
                            <img src="{{$about->img_url}}" width="100%" height="90%" class="p-0 m-0" alt="">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 offset-xl-2 offset-lg-2  offset-md-0 offset-sm-0 p-5">
                            <div class="we-section-two-inner  d-flex justify-content-center align-items-center flex-column w-100 h-100 p-5">
                                <h4 class="pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 pb-2">{{$about->title}}</h4>
                                <p>
                                    {{$about->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row p-0 m-0">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 p-5">
                            <div class="we-section-two-inner2 d-flex justify-content-center align-items-center flex-column  w-100 h-100 p-5">
                                <h4 class="pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 pb-2">{{$about->title}}</h4>
                                <p>
                                    {{$about->description}}
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 offset-xl-2 offset-lg-2  offset-md-0 offset-sm-0 py-5 px-0">
                            <img src="{{$about->img_url}}" width="100%" height="90%" class="p-0 m-0" alt="">
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        @foreach ($abouts as $key =>  $about)
            <div class="we-section-two-mobile">
                <div class="row p-0 m-0 ">
                    <div class="col-12 py-3">
                        <div class="we-section-two-mobile-inner d-flex justify-content-center align-items-center">
                            <img src="{{$about->img_mobil_url}}" alt="" width="100%" height="100%">
                        </div>
                        <div class="we-section-two-mobile-items p-5 d-flex justify-content-center align-items-center flex-column">
                            <h4 class="pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 pb-2">{{$about->title}}</h4>
                            <p>
                                {{$about->description}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
@endsection
