@extends('layouts.app')

@section('content')
    <section>
        <div class="section-first w-100 h-100">
            <div class="section-first-inner">
                <div class="section-first-inner-text">
                    <h1>Brooklyn online <br>education center</h1>
                    <p>Учи английский онлайн</p>
                    <button class="btn1">
                        <a href="{{url('lessons')}}"
                           class="text-white d-flex justify-content-center align-items-center">
                            Начать обучение <i class="material-icons pt-1">keyboard_arrow_right</i>
                        </a>
                    </button>
                    <button class="btn2">
                        <a href="{{url('tests')}}" class="text-dark">Узнать свой уровень </a>
                    </button>
                    <img src="{{asset('client/images/book1.png')}}" class="book-img" alt="">
                </div>
                <img src="{{asset('client/images/Group.png')}}" class="img2" alt="">
                <img src="{{$image && $image->home_img ?  $image->home_img_url : asset('client/images/person.png')}}"
                     class="img" alt="">
                <img src="{{$image && $image->home_img ?  $image->home_img_url : asset('client/images/person2.png')}}"
                     class="img-mobile d-flex" alt="">
            </div>
        </div>

        <div class="section-second">
            <img class="section-second-img" src="{{asset('client/images/bg.png')}}" width="100%" height="100%" alt="">
            <div class="section-second-inner px-5 pb-5 w-100 h-100">
                <h1 class="pt-5 d-flex justify-content-center align-items-center">Наши курсы подходят</h1>
                <div class="container">
                    <div class="con">
                        <div class="row p-0 m-0 d-flex justify-content-center align-items-center">
                            <div class="col-4">
                                <div class="section-second-inner-items">
                                    <img src="{{asset('client/images/1.png')}}" alt="">
                                    <p>Для семейнего обучения онлайн.</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="section-second-inner-items">
                                    <img src="{{asset('client/images/2.png')}}" alt="">
                                    <p>Для тех кто хочет обучится Английскому языку не выходя из дома.</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="section-second-inner-items">
                                    <img src="{{asset('client/images/3.png')}}" alt="">
                                    <p>Для тех кто любит учится всегда.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="carouselExampleIndicators"
                     class="carousel slide p-5 d-flex justify-content-center align-items-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{asset('client/images/1.png')}}" alt="images/1.png" width="100%" height="auto">
                            <div class="carousel-caption d-none d-md-block">
                                <p class="mobil-carousel-text text-white">Для семейнего обучения онлайн.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('client/images/2.png')}}" alt="images/1.png" width="100%" height="auto">
                            <div class="carousel-caption d-none d-md-block">
                                <p class="mobil-carousel-text text-white">Для тех кто хочет обучится Английскому языку
                                    не
                                    выходя из дома.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('client/images/3.png')}}" alt="images/1.png" width="100%" height="auto">
                            <div class="carousel-caption d-none d-md-block">
                                <p class="mobil-carousel-text text-white">Для тех кто любит учится всегда.</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="section-third w-100 h-100 pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0">
            <div
                class="section-third-inner w-100 h-100 pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0">
                <h1 class="d-flex justify-content-center">5 причин почему Brooklyn</h1>
                <div class="row p-0 m-0 pt-5 mt-5">
                    <div class="col-6 p-0 m-0">
                        <img src="{{asset('client/images/man.png')}}" width="100%" height="auto" alt="">
                    </div>
                    <div class="col-6">
                        <div class="section-third-inner-img1 d-flex justify-content-center">
                            <img src="{{asset('client/images/Reason 1.png')}}" width="50%" height="50%" alt="">
                        </div>
                        <div class="section-third-inner-img2 d-flex justify-content-between">
                            <img src="{{asset('client/images/Reason 3.png')}}" width="50%" height="50%" alt="">
                            <img src="{{asset('client/images/Reason 2.png')}}" width="50%" height="50%" alt="">
                        </div>
                        <div class="section-third-inner-img3 d-flex">
                            <img src="{{asset('client/images/Reason 5.png')}}" width="50%" height="50%" alt="">
                            <img src="{{asset('client/images/Reason 4.png')}}" width="50%" height="50%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-four w-100 h-100 pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0">
            <div
                class="section-four-inner pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0 w-100 h-100 d-flex justify-content-center align-items-center">
                <img src="{{asset('client/images/bg1.png')}}" width="100%" height="100%"
                     class="pos-a d-xl-block d-lg-block d-md-none d-sm-none"
                     alt="">
                <div class="container section-four-inner">
                    <div class="row p-0 m-0">
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/6.png')}}" alt="">
                        </div>
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/7.png')}}" alt="">
                        </div>
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/8.png')}}" alt="">
                        </div>
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/9.png')}}" alt="">
                        </div>
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/10.png')}}" alt="">
                        </div>
                        <div
                            class="col-xl-2 col-lg-2 col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                            <img src="{{asset('client/images/11.png')}}" alt="">
                        </div>
                    </div>
                    <div class="d-flex mt-5 justify-content-center align-items-center">
                        <a href="{{url('lessons')}}"
                           class="text-dark btn3 d-flex justify-content-center align-items-center">
                            Перейти к занятиям <i class="material-icons pt-1">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-five w-100 h-100 pb-5 pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0">
            <div class="section-five-inner pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 mt-xl-5 mt-lg-5 mt-md-0 mt-sm-0">
                <h1 class="d-flex justify-content-center align-items-center">Чему мы вас обучим?</h1>
                <div class="container section-five-item pt-5">
                    <div class="row p-0 m-0 pt-5 ">
                        @foreach ($subjects as $subject)
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                <div
                                    class="section-five-inner-items d-flex justify-content-center align-items-center flex-column">
                                    <img style="max-height: 175px" src="{{$subject->img_url}}" alt="">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <a href="{{url('price')}}" class="btn4 mt-4">Оформить подписку</a>
                                        <a href="{{url('lesson/'.$subject->id)}}" class="btn5 mt-4">
                                            Попробовать бесплатно
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="section-five-item2">
                    <section class="lazy slider" data-sizes="50vw">
                        @foreach ($subjects as $subject)
                            <div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div
                                        class="section-five-inner-items d-flex justify-content-center align-items-center flex-column">
                                        <img src="{{$subject->img_url}}" alt="">
                                        <a href="{{url('price')}}" class="btn4 mt-4">Оформить подписку</a>
                                        <a href="{{url('lesson/'.$subject->id)}}" class="btn5 mt-4">
                                            Попробовать бесплатно
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </section>
                </div>
            </div>
        </div>

        <div class="section-six py-5 w-100 h-100 ">
            <div class="section-six-inner w-100 h-100">
                <h1 class="d-flex justify-content-center align-items-center px-3">
                    Отзывы от учеников и их Родителей
                </h1>
                <div class="container mt-5">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link tabs-btn1 active" id="pills-home-tab" data-toggle="pill"
                               href="#pills-home"
                               role="tab" aria-controls="pills-home" aria-selected="true">Родители</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link tabs-btn2" id="pills-profile-tab" data-toggle="pill"
                               href="#pills-profile"
                               role="tab" aria-controls="pills-profile" aria-selected="false">Дети</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                             aria-labelledby="pills-home-tab">
                            <div class="row pt-5 mt-5">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-5">
                                    <div class="section-six-inner-carousel p-5">
                                        <div class="section-six-inner-carousel-img">
                                            <img src="{{asset('client/images/El.png')}}" alt="">
                                        </div>
                                        <div class="section-six-inner-carousel-htext">
                                            <h3>Наталья, Александровна</h3>
                                        </div>
                                        <div class="section-six-inner-carousel-ptext">Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Sit tortor semper integer fringilla mauris
                                            cursus
                                            nec tellus. Vitae at in nibh egestas. Viverra aliquam vel viverra
                                            pellentesque
                                            massa. Vestibulum lectus donec imperdiet varius pellentesque at lorem
                                            aliquet.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                    <div class="section-six-inner-carousel p-5">
                                        <div class="section-six-inner-carousel-img">
                                            <img src="{{asset('client/images/El.png')}}" alt="">
                                        </div>
                                        <div class="section-six-inner-carousel-htext">
                                            <h3>Наталья, Александровна</h3>
                                        </div>
                                        <div class="section-six-inner-carousel-ptext">Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Sit tortor semper integer fringilla mauris
                                            cursus
                                            nec tellus. Vitae at in nibh egestas. Viverra aliquam vel viverra
                                            pellentesque
                                            massa. Vestibulum lectus donec imperdiet varius pellentesque at lorem
                                            aliquet.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="row pt-5 mt-5">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-5">
                                    <div class="section-six-inner-carousel p-5">
                                        <div class="section-six-inner-carousel-img">
                                            <img src="{{asset('client/images/El1.png')}}" alt="">
                                        </div>
                                        <div class="section-six-inner-carousel-htext">
                                            <h3>Наталья, Александровна</h3>
                                        </div>
                                        <div class="section-six-inner-carousel-ptext">Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Sit tortor semper integer fringilla mauris
                                            cursus
                                            nec tellus. Vitae at in nibh egestas. Viverra aliquam vel viverra
                                            pellentesque
                                            massa. Vestibulum lectus donec imperdiet varius pellentesque at lorem
                                            aliquet.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                    <div class="section-six-inner-carousel p-5">
                                        <div class="section-six-inner-carousel-img">
                                            <img src="{{asset('client/images/El1.png')}}" alt="">
                                        </div>
                                        <div class="section-six-inner-carousel-htext">
                                            <h3>Наталья, Александровна</h3>
                                        </div>
                                        <div class="section-six-inner-carousel-ptext">Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Sit tortor semper integer fringilla mauris
                                            cursus
                                            nec tellus. Vitae at in nibh egestas. Viverra aliquam vel viverra
                                            pellentesque
                                            massa. Vestibulum lectus donec imperdiet varius pellentesque at lorem
                                            aliquet.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-seven pt-xl-5 pt-lg-5 pt-md-0 pt-sm-0 w-100 h-100">
            <div class="section-seven-inner w-100 h-100">
                <div class="container footer-container">
                    <h1 class="d-flex justify-content-center align-items-center">Остались вопросы?</h1>
                    <div class="row p-0  d-flex justify-content-center align-items-center">
                        <div
                            class="col-xl-8 col-lg-8 col-md-12 col-sm-12 offset-xl-1 offset-lg-2 offset-md-0 offset-sm-0 section-seven-inner-img">
                            <img src="{{asset('client/images/fot-bg.png')}}" width="100%" height="auto" alt="">
                            <img src="{{asset('client/images/bg-12.png')}}" class="bg-12" width="100%" height="auto"
                                 alt="">
                            <div class="section-seven-inner-item d-flex justify-content-end">
                                <p> Можете задать нам вопрос прямо сейчас! Мы ответим в течение 1 рабочего дня.</p>
                            </div>
                            <div class="section-seven-inner-item2">
                                <form action="{{url('site/question')}}" method="POST">
                                    @csrf
                                    <div class="form-row d-flex justify-content-center align-items-center">
                                        <div class="col-md-8 mb-4">
                                            <input name="full_name" type="text" placeholder="Имя" required
                                                   class="form-control @error('full_name') is-invalid @enderror w-100">
                                            @error('full_name')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>

                                        <div class="col-md-8 mb-4">
                                            <input name="email" type="email" placeholder="Email" required
                                                   class="form-control @error('email') is-invalid @enderror w-100">
                                            @error('email')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="col-md-8 mb-4">
                                            <textarea name="description" id="" rows="7" style="margin-top: 7px;"
                                                      required
                                                      class="w-100 form-control @error('email') is-invalid @enderror"></textarea>
                                            @error('email')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <button>Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
@endsection
