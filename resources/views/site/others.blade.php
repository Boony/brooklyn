@extends('layouts.app')

@section('content')
    <section>
        <div class="other-section-one p-0 m-0 p-5">
            <div class="other-section-one-inner  pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h1>Полезное</h1>
            </div>
        </div>
        <div class="container">
            <div class="row p-0 m-0 py-5">
                @foreach ($news as $new)
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                        <div class="other-section-one-inner-items d-flex justify-content-center align-items-center flex-column">
                            <img src="{{ $new->img_url }}" width="100%" height="auto" alt="">
                            <h4>{{$new->title}}</h4>
                            <p>{{$new->short}}</p>
                            <a href="{{url('news/'.$new->id)}}" class="d-flex justify-content-center align-items-center">Перейти <i
                                    class="material-icons d-flex justify-content-end">keyboard_arrow_right</i></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
