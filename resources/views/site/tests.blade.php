@extends('layouts.app')

@section('content')
    <section>
        <div class="price-section-one p-0 m-0 p-5">
            <div class="price-section-one-inner  pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h1>Tests</h1>
            </div>
        </div>

        @if(count($tests) > 0)
            <div class="container">
                <form action="{{url('check')}}" method="post">
                    @csrf
                    <div class="row p-0 m-0 py-5">
                        @if(count($errors))
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                                <div class="alert alert-danger" role="alert">
                                    Check at least one question
                                </div>
                            </div>
                        @endif
                        @if(Session::has('answer'))
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('answer')}}
                                    @php(\Session::remove('answer'))
                                </div>
                            </div>
                        @endif
                        @foreach ($tests as $key => $test)
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                                <h2>{{$test->question}}</h2>
                                <ul class="list-group pt-1">
                                    @foreach ($test->answers as $key => $answer)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <label for="{{$test->id.'_'.$answer->id}}"
                                                   style="margin-bottom: 0; cursor: pointer">{{$answer->answer}}
                                            </label>
                                            <span class="badge">
                                            <input type="radio" id="{{$test->id.'_'.$answer->id}}"
                                                   name="answer[{{$test->id}}]" value="{{$answer->id}}">
                                        </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                            <button type="submit" class="btn btn-primary">Check</button>
                        </div>
                    </div>
                </form>
            </div>
        @endif
    </section>
@endsection
