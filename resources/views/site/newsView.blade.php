@extends('layouts.app')

@section('content')
    <section>
        <div class="other-section-one p-0 m-0 p-5">
            <div class="other-section-one-inner  pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h3>{{$news->title}}</h3>
            </div>
        </div>
        <div class="container">
            <div class="row p-0 m-0 py-5">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                    <div
                        class="other-section-one-inner-items d-flex justify-content-center align-items-center flex-column">
                        <img src="{{ $news->img_url }}" width="100%" height="auto" alt="">
                        <p>{{$news->description}}</p>
                    </div>
                </div>
            </div>
        </div>
        @if(count($news->files) > 0)
            <div class="container">
                <div class="row p-0 m-0 py-5">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-xl-5 p-md-0 p-sm-0 pt-5">
                        <h2>Useful materials</h2>
                        <ul class="list-group pt-1">
                            @foreach ($news->files as $key => $file)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{$key+1}}. {{$file->title}}

                                    <span class="badge badge-primary">
                                        <a href="{{$file->link}}" download="{{$file->title}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-download"><path
                                                    d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"/><polyline
                                                    points="7 10 12 15 17 10"/><line x1="12" y1="15" x2="12"
                                                                                     y2="3"/></svg>
                                        </a>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </section>
@endsection
