@extends('layouts.app')

@section('content')
    <section>
        <div class="price-section-one p-0 m-0 p-5">
            <div class="price-section-one-inner  pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h1>стоимость обучения</h1>
            </div>
        </div>
        @foreach ($subjects as $subject)
            @if (count($subject->prices) > 0)
                <div class="container price-con-inner">
                    <h1 class="price-text w-100 d-flex justify-content-center align-items-center p-0 m-0">
                        {{$subject->name}}
                    </h1>
                    <div class="row p-0 m-0 py-5">
                        @foreach ($subject->prices as $price)
                            <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 py-5">
                                <div
                                    class="price-section-one-item d-flex justify-content-center align-items-center flex-column w-100 h-100 pb-5">
                                    <h3>{{ $price->title  }}</h3>
                                    <div class="d-flex justify-content-center align-items-center flex-column pt-3">
                                        <h6>{{$price->month}} Месяц</h6>
                                        <h2>{{ number_format($price->price, 0, '.', ' ') }} сум</h2>
                                    </div>
                                    <div class="d-flex justify-content-center align-items-center flex-column pt-3">
                                        <ul>
                                            @foreach ($price->items as $item)
                                                <li>{{$item->item->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @if ($contact)
                                        <div class="d-flex justify-content-center align-items-center flex-column pt-3">
                                            <a href="tel:+{{$contact->phone}}" class="conteacher text-white">
                                                <i class="material-icons text-white mr-2">phone</i>
                                                Contact with teacher.
                                            </a>
                                        </div>
                                    @endif
                                    @guest()
                                        <a href="{{url('login')}}" class="price-pay">
                                            Оформить подписку
                                        </a>
                                    @elseauth()
                                        <a href="{{url('site/payment/'.$price->id)}}" class="price-pay">
                                            Оформить подписку
                                        </a>
                                    @endguest
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        @endforeach
    </section>
@endsection
