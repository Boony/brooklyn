<style>
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: 20px !important;
    }
</style>


@extends('layouts.app')

@section('content')
    <section>
        <div class="video-section-one p-0 m-0 p-5">
            <div class="video-section-one-inner  pt-5 px-xl-5 px-lg-5 px-md-0 px-sm-0 p-0 m-0">
                <h1>Видео-уроки</h1>
            </div>
        </div>
        <div class="video-section-two d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row pt-5 p-0 m-0 ">
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div class="row pt-5 p-0 m-0 ">
                            @foreach ($subjects as $subject)
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 pt-5">
                                    <img src="{{$subject->img_url}}" width="100%" height="auto" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-6 col-sm-12 d-flex pt-5 flex-column">
                        <form action="{{url('lessons')}}" method="get" id="videoForm">
                            <div class="accordion" id="accordionExampleS">
                                <div class="card">
                                    <div class="card-header" id="headingOneS">
                                        <h2 class="mb-0">
                                            <span
                                                class="btn btn-link text-white d-flex justify-content-center align-items-center w-100"
                                                type="span" data-toggle="collapse" data-target="#collapseOneS"
                                                aria-expanded="true"
                                                aria-controls="collapseOneS">
                                                Subjects <i class="material-icons">keyboard_arrow_down</i>
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapseOneS" class="collapse show" aria-labelledby="headingOneS"
                                         data-parent="#accordionExampleS">
                                        <div class="card-body">
                                            <div class="new">
                                                @foreach ($subjects as $subject)
                                                    <label
                                                        class="option d-flex justify-content-center align-items-center">
                                                        <input type="radio" value="{{$subject->id}}"
                                                               name="subject_id" required>{{$subject->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <span
                                                class="btn btn-link text-white d-flex justify-content-center align-items-center w-100"
                                                type="span" data-toggle="collapse" data-target="#collapseOne"
                                                aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Level <i class="material-icons">keyboard_arrow_down</i>
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="new">
                                                @foreach ($levels as $level)
                                                    <label
                                                        class="option d-flex justify-content-center align-items-center">
                                                        <input type="radio" value="{{$level->id}}"
                                                               name="level_id" required>{{$level->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="accordion" id="accordionExample1">
                                <div class="card">
                                    <div class="card-header" id="headingOne1">
                                        <h2 class="mb-0">
                                            <span
                                                class="btn btn-link text-white d-flex justify-content-center align-items-center w-100"
                                                type="span" data-toggle="collapse" data-target="#collapseOne1"
                                                aria-expanded="true"
                                                aria-controls="collapseOne1">
                                                Lessons <i class="material-icons">keyboard_arrow_down</i>
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1"
                                         data-parent="#accordionExample1">
                                        <div class="card-body">
                                            <div class="new">
                                                @foreach ($lessons as $lesson)
                                                    <label
                                                        class="option d-flex justify-content-center align-items-center">
                                                        <input type="radio" value="{{$lesson->lesson}}"
                                                               name="lesson" required>Lesson-{{$lesson->lesson}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="begin-lesson w-100 d-flex justify-content-center align-items-center">
                                <button class="d-flex justify-content-center" type="submit">
                                    Начать обучение<i class="material-icons">play_arrow</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('customScript')
    <script type="text/javascript">
        function submit() {
            $('#videoForm').submit();
        }
    </script>
@endsection
