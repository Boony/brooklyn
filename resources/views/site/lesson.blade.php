@extends('layouts.app')

@section('content')
    <section>
        @if ($lesson)
            <div class="lesson-section-one p-0 m-0 p-5">
                <div class="lesson-section-one-inner p-5 p-0 m-0">
                    <h1>Lesson-{{$lesson->lesson}}</h1>
                </div>
            </div>
            <div class="lesson-section-items w-100 h-100">
                <div class="row p-0 m-0">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <video width="100%" height="auto" controls>
                            <source src="{{$lesson->video_uz_url}}" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 d-flex justify-content-center align-items-center">
                        <img src="{{asset('client/images/bg-we.png')}}" width="100%" height="auto" alt="">
                        <div style="top: 45px !important;"
                             class="lesson-section-items-text p-xl-5 p-lg-5 p-md-0 p-sm-0 text-white d-flex justify-content-center align-items-center flex-column ">
                            <h2>{{$lesson->title_uz}}</h2>
                            <p class="px-5 py-2 d-flex justify-content-center align-items-center">
                                {{$lesson->description_uz}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lesson-section-items-second w-100 h-100 mt-5">
                <div class="row p-0 m-0">
                    @if ($lesson->test)
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                            <h1 class="">Online Test</h1>
                            <h3><b>{{$lesson->test->question}}</b></h3>
                            @foreach ($lesson->test->answers as $key => $answer)
                                <p>{{$key+1}}: {{$answer->answer}}</p>
                            @endforeach
                        </div>
                    @endif
                    @if ($lesson->homework_uz)
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  ">
                            <h1 class="">Homework</h1>
                            <p>{{$lesson->homework_uz}}</p>
                        </div>
                    @endif
                </div>
                <div class="begin-lesson d-flex justify-content-between container my-5">
                    <div class="try-again">
                        @if ($prev)
                            <a href="{{url('lessons?subject_id='.$prev->subject_id.'&level_id='.$prev->level_id.'&lesson='.$prev->lesson)}}">Prev</a>
                        @endif
                    </div>
                    <div class="next">
                        @if ($next)
                            <a href="{{url('lessons?subject_id='.$next->subject_id.'&level_id='.$next->level_id.'&lesson='.$next->lesson)}}">Next</a>
                        @endif
                    </div>
                </div>
            </div>
        @else

            <div class="lesson-section-one p-0 m-0 p-5">
                <div class="lesson-section-one-inner p-5 p-0 m-0">
                    <h1>No Lesson</h1>
                </div>
            </div>
        @endif
    </section>
@endsection
