@extends('layouts.app')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="container">
                <div class="formBox">
                    <form action="{{url('login')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <h1>Login form</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <div class="inputBox ">
                                    <div class="inputText">Username</div>
                                    <input type="text" name="username" class="input" required
                                           value="{{ old('username') }}">
                                    @error('username')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-8 offset-sm-2">
                                <div class="inputBox">
                                    <div class="inputText">Password</div>
                                    <input type="password" name="password" class="input" required>
                                    @error('password')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <a href="{{url('register')}}">
                                    <span style="color: #03A9F4;">Register, if you don't have an account</span>
                                </a>
                            </div>
                            <div class="col-sm-8 offset-sm-2">
                                <button type="submit" class="button">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('customJs')
    <script type="text/javascript">
        $(".input").focus(function () {
                $(this).parent().addClass("focus");
            }
        );
    </script>
@endsection
