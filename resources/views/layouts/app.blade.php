<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Brooklyn.uz</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('client/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('client/slick/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('client/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('client/css/custom.css')}}">

    <style type="text/css">
        .begin-lesson button {
            width: 100%;
            height: auto;
            text-align: center;
            padding: 12px;
            background: #DE6565;
            border-radius: 82px;
        }

        .other-section-one-inner h3 {
            width: 603px;
            height: 252px;
            left: 165px;
            top: 178px;
            font-family: Montserrat;
            font-style: normal;
            font-weight: bold;
            font-size: 45px;
            line-height: 85px;
            text-transform: uppercase;
            color: #FFFFFF;
        }


        .invalid-feedback {
            display: block;
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #dc3545;
        }

    </style>

</head>
<body>


<header class="top-header-bg">
    <section class="navigation">
        <div class="nav-container">
            <div class="brand d-flex justify-content-start">
                <h4><a href="{{url('/')}}" class="text-white">Brooklyn.uz</a></h4>
            </div>
            <nav>
                <div class="nav-mobile">
                    <a id="nav-toggle" href="javascript:void(0)"><span></span></a>
                </div>
                <ul class="nav-list">
                    <li><a href="{{url('/')}}">Главная </a></li>
                    <li><a href="{{url('about')}}">О нас</a></li>
                    <li><a href="{{url('price')}}">Стоимость</a></li>
                    <li><a href="{{url('video')}}">Видеоуроки</a></li>
                    <li><a href="{{url('others')}}">Полезное</a></li>
                    <li><a href="{{url('news')}}">Доска объявлений</a></li>
                    @if ($contact->phone)
                        <li>
                            <a href="tel:+{{$contact->phone}}">
                                <i class="material-icons text-white pt-xl-3 pt-lg-3 pt-md-0 pt-sm-0">phone_forwarded</i>
                            </a>
                        </li>
                    @endif
                    @if (auth()->guest())
                        <li>
                            <a href="{{url('login')}}">
                                <i class="material-icons text-white pt-xl-3 pt-lg-3 pt-md-0 pt-sm-0">input</i>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="{{url('profile')}}">
                                <i class="material-icons text-white pt-xl-3 pt-lg-3 pt-md-0 pt-sm-0">account_box</i>
                            </a>
                        </li>
                    @endif

                    {{--                    <li>--}}
                    {{--                        <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">--}}
                    {{--                            <i class="material-icons text-white pt-xl-3 pt-lg-3 pt-md-0 pt-sm-0">input</i>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog"--}}
                    {{--                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">--}}
                    {{--                        <div class="modal-dialog modal-dialog-centered" role="document">--}}
                    {{--                            <div class="modal-content">--}}
                    {{--                                <div class="modal-header">--}}
                    {{--                                    <h5 class="modal-title text-white" id="exampleModalLongTitle">Tell Us</h5>--}}
                    {{--                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--                                        <span aria-hidden="true" class="text-white">&times;</span>--}}
                    {{--                                    </button>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="modal-body">--}}
                    {{--                                    <div class="d-flex justify-content-center align-items-center">--}}
                    {{--                                        <input class="w-100 mx-5" type="text" placeholder="Name" required="">--}}
                    {{--                                    </div>--}}
                    {{--                                    <div class="d-flex justify-content-center align-items-center">--}}
                    {{--                                        <input class="w-100 mx-5" type="text" placeholder="Фамилия" required="">--}}
                    {{--                                    </div>--}}
                    {{--                                    <div class="d-flex justify-content-center align-items-center">--}}
                    {{--                                        <input class="w-100 mx-5" type="text" placeholder="Ваш номер">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="modal-footer">--}}
                    {{--                                    <button type="button" class="btn btn-white">Send</button>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </ul>
            </nav>
        </div>
    </section>
</header>
@yield('content')
<footer class="bottom-footer-bg pt-5">
    <div class="bottom-footer-inner pt-5">
        <div class="row p-0 m-0 pt-5 mt-5 bottom-footer-inner-big-item">
            <div class="col-2 offset-1 py-5 mt-5">
                <div class="bottom-footer-inner-items">
                    <ul class="p-2">
                        <li><a href="{{url('/')}}">Главная </a></li>
                        <li><a href="{{url('about')}}">О нас</a></li>
                        <li><a href="{{url('price')}}">Стоимость</a></li>
                        <li><a href="{{url('video')}}">Видеоуроки</a></li>
                        <li><a href="{{url('others')}}">Полезное</a></li>
                        <li><a href="{{url('news')}}">Доска объявлений</a></li>
                        <li><a href="javascript:void(0)"><i class="material-icons text-white ">person</i></a></li>
                        <li><a href="javascript:void(0)"><i class="material-icons text-white ">input</i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-2 py-5 mt-5 ">
                <div class="bottom-footer-inner-items">
                    <ul class="p-2">
                        @foreach ($subjects as $subject)
                            <li><a href="{{url('lesson/'.$subject->id)}}">{{$subject->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-2 py-5 mt-5">
                <div class="bottom-footer-inner-items">
                    <ul class="p-2">
                        <li><a href="#!">SPEAKING</a></li>
                        <li><a href="#!">LIBRARY</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-5 pt-5 mt-5">
                <div class="bottom-footer-inner-search px-5">
                    <p class="text-white">Оформить подписку</p>
                    <form action="{{url('subscriber')}}" method="POST" class="subscriber">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" name="email" required class="form-control" placeholder="Email"
                                   aria-label="Email"
                                   aria-describedby="basic-addon2">
                            <div class="input-group-append">
                            <span class="input-group-text bg-primary" id="basic-addon2">
                                <button class="btn btn-sm bg-primary">
                                    <i class="material-icons text-white ">search</i>
                                </button>
                            </span>
                            </div>
                        </div>
                    </form>
                    <div class="d-flex align-items-center pt-4">
                        @if ($contact->instagram)
                            <div class="circle2 mx-2  d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->instagram}}" target="_blank" class="text-white">
                                    <i class="fab mx-2  fa-instagram"></i>
                                </a>
                            </div>
                        @endif
                        @if ($contact->facebook)
                            <div class="circle1  mx-2 d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->facebook}}" target="_blank" class="text-white">
                                    <i class="fab mx-2  fa-facebook-f"></i>
                                </a>
                            </div>
                        @endif
                        @if ($contact->telegram)
                            <div class="circle3  mx-2 d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->telegram}}" class="text-white">
                                    <i class="fab mx-2  fa-telegram-plane"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="d-flex align-items-center text-white py-4">
                        <h1>Brooklyn.uz</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-0 m-0 bottom-footer-inner-small-item p-3 pt-5">
            <div class="col-12">
                <div class="bottom-footer-inner-search px-5">
                    <p class="text-white">Оформить подписку</p>
                    <form action="{{url('subscriber')}}" method="POST" class="subscriber">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" name="email" required class="form-control" placeholder="Email"
                                   aria-label="Email"
                                   aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-sm bg-primary">
                                    <span class="input-group-text bg-primary" id="basic-addon2">
                                        <i class="material-icons text-white ">search</i>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="d-flex align-items-center pt-4">
                        @if ($contact->instagram)
                            <div class="circle2 mx-2  d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->instagram}}" target="_blank" class="text-white">
                                    <i class="fab mx-2  fa-instagram"></i>
                                </a>
                            </div>
                        @endif
                        @if ($contact->facebook)
                            <div class="circle1  mx-2 d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->facebook}}" target="_blank" class="text-white">
                                    <i class="fab mx-2  fa-facebook-f"></i>
                                </a>
                            </div>
                        @endif
                        @if ($contact->telegram)
                            <div class="circle3  mx-2 d-flex justify-content-center align-items-center text-white">
                                <a href="{{$contact->telegram}}" class="text-white">
                                    <i class="fab mx-2  fa-telegram-plane"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="d-flex align-items-center text-white py-4">
                        <h1>Brooklyn.uz</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script defer src="{{asset('client/fontawesome/js/brands.js')}}"></script>
<script defer src="{{asset('client/fontawesome/js/solid.js')}}"></script>
<script defer src="{{asset('client/fontawesome/js/fontawesome.js')}}"></script>
<script defer src="{{asset('client/slick/slick.js')}}"></script>
<script defer src="{{asset('client/js/index.js')}}"></script>

@yield('customJs')

</body>
</html>
