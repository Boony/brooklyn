<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Brooklyn | Admin Panel </title>
    <link rel="icon" type="image/x-icon" href="{{asset('/admin/assets/img/favicon.ico')}}"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{asset('/admin/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/admin/assets/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <style>

        /*
            Just for demo purpose ---- Remove it.
        */
        /*<starter kit design>*/

        .widget-one {

        }

        .widget-one h6 {
            font-size: 20px;
            font-weight: 600;
            letter-spacing: 0px;
            margin-bottom: 22px;
        }

        .widget-one p {
            font-size: 15px;
            margin-bottom: 0;
        }

        .tooltip {
            left: 12px !important;
        }

        .invalid-feedback-custom {
            color: #e7515a;
            font-size: 13px;
            font-weight: 700;
            letter-spacing: 1px;
        }

        /*/<starter kit design>*/

    </style>

    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/file-upload/file-upload-with-preview.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/plugins/editors/quill/quill.snow.css')}}">

</head>
<body>

<!--  BEGIN NAVBAR  -->
@include('partials.header')
<!--  END NAVBAR  -->

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="cs-overlay"></div>
    <div class="search-overlay"></div>

@include('partials.slider')

<!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            @yield('content')
        </div>
    </div>
    <!--  END CONTENT AREA  -->

</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset('admin/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('admin/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('admin/assets/js/app.js')}}"></script>
<script>
    $(document).ready(function () {
        App.init();
    });
    $('[data-toggle="tooltip"]').tooltip()
</script>
<script src="{{asset('admin/assets/js/custom.js')}}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->


<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{asset('/admin/plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
<script src="{{asset('/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
<script src="{{asset('/admin/plugins/bootstrap-maxlength/custom-bs-maxlength.js')}}"></script>
<script src="{{asset('/admin/plugins/editors/markdown/simplemde.min.js')}}"></script>
{{--<script src="{{asset('/admin/plugins/editors/markdown/custom-markdown.js')}}"></script>--}}
<script src="{{asset('/admin/plugins/editors/quill/quill.js')}}"></script>
{{--<script src="{{asset('/admin/plugins/editors/quill/custom-quill.js')}}"></script>--}}

@yield('customScript')

</body>
</html>
