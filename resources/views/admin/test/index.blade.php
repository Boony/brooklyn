@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Main page tests</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Question</th>
                                <th class="text-center">A</th>
                                <th class="text-center">B</th>
                                <th class="text-center">C</th>
                                <th class="text-center">D</th>
                                <th class="text-center">

                                    <a href="{{url('admin-panel/main-test/create')}}"
                                       class="badge outline-badge-info shadow-none">
                                        Create
                                    </a>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($tests))
                                @foreach ($tests as $item)
                                    <tr class="">
                                        <td>{{$item->question}}</td>
                                        @foreach($item->answers as $answer)
                                            <td class="{{ $answer->is_true ? 'btn-success' : '' }}">{{$answer->answer}} </td>
                                        @endforeach
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <form action="{{url('admin-panel/main-test/'.$item->id)}}"
                                                      method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" data-toggle="tooltip" data-placement="top"
                                                            title="Delete"
                                                            class="btn btn-link check-form"
                                                    >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                             height="24" viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2"
                                                             stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-trash-2">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
