@extends('layouts.admin')
@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Create main page tests</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/main-test')}}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-12 mb-4">
                                <label for="question">Question</label>
                                <textarea id="question" name="question"
                                          class="form-control @error('question') is-invalid @enderror"
                                          rows="5"></textarea>
                                @error('question')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="a">A</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.0') is-invalid @enderror"
                                               id="a"
                                               placeholder="A"
                                               maxlength="255">
                                        @error('test.0')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio1"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio1" name="is_true"
                                                   class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="customRadio1">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="b">B</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.1') is-invalid @enderror"
                                               id="b"
                                               placeholder="B"
                                               maxlength="255">
                                        @error('test.1')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio2"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio2" name="is_true"
                                                   class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="customRadio2">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="c">C</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.2') is-invalid @enderror"
                                               id="c"
                                               placeholder="C"
                                               maxlength="255">
                                        @error('test.2')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio3"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio3" name="is_true"
                                                   class="custom-control-input" value="2">
                                            <label class="custom-control-label" for="customRadio3">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="d">D</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.3') is-invalid @enderror"
                                               id="d"
                                               placeholder="D"
                                               maxlength="255">
                                        @error('test.3')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio4"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio4" name="is_true"
                                                   class="custom-control-input @error('is_true') is-invalid @enderror"
                                                   value="3">
                                            <label class="custom-control-label" for="customRadio4">Is true</label>
                                            @error('is_true')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
