@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div id="custom_styles" class="col-lg-12 layout-spacing col-md-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Page images</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/page-image')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$image->id}}">
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <div class="custom-file-container" data-upload-id="myFirstImage">
                                    <label>Upload Image Home page image (Single File)
                                        <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                           title="Clear Image">x</a>

                                        @error('home_img') <span style="color: #e7515a">{{$message}}</span> @enderror
                                    </label>
                                    <label class="custom-file-container__custom-file">
                                        <input type="file" name="home_img"
                                               class="custom-file-container__custom-file__custom-file-input"
                                               accept="image/*">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                    </label>
                                    <div class="custom-file-container__image-preview"></div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        let firstUpload = new FileUploadWithPreview('myFirstImage',{
            images: {
                baseImage: '{{$image->home_img_url}}',
            },
        })
    </script>
@endsection
