@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div id="custom_styles" class="col-lg-12 layout-spacing col-md-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Profile</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/profile')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="name">Name</label>
                                <input type="text" name="name"
                                       class="form-control placement-bottom-right @error('name') is-invalid @enderror"
                                       id="name"
                                       value="{{$user->name}}"
                                       placeholder="Name"
                                       maxlength="255">
                                @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="phone">Email</label>
                                <input type="email" name="email"
                                       class="form-control placement-bottom-right @error('email') is-invalid @enderror"
                                       id="email"
                                       placeholder="Email"
                                       value="{{$user->email}}"
                                       maxlength="255">
                                @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="password">Password</label>
                                <input type="password" name="password"
                                       class="form-control placement-bottom-right @error('password') is-invalid @enderror"
                                       id="password"
                                       placeholder="Password"
                                       maxlength="255"
                                >
                                @error('password')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="password_confirm">Password Confirm</label>
                                <input type="password" name="password_confirm"
                                       class="form-control placement-bottom-right @error('password_confirm') is-invalid @enderror"
                                       id="password_confirm"
                                       placeholder="Password Confirm"
                                       maxlength="255"
                                >
                                @error('password_confirm')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
