@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>
    <div class="row">
        <div id="custom_styles" class="col-lg-12 layout-spacing col-md-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>News form</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/news/'.$news->id)}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="type" value="{{$news->type}}">
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="validationCustom01">Title</label>
                                <input type="text" name="title"
                                       class="form-control placement-bottom-right @error('title') is-invalid @enderror"
                                       id="validationCustom01"
                                       placeholder="Title"
                                       maxlength="225"
                                       value="{{$news->title}}"
                                >
                                @error('title')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="textarea">Short</label>
                                <textarea id="textarea" name="short"
                                          class="form-control @error('short') is-invalid @enderror"
                                          maxlength="225"
                                          rows="5">{{$news->short}}</textarea>
                                @error('short')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-4">
                                <label for="demo1">Description</label>
                                <textarea id="demo1" name="description">{{$news->description}}</textarea>
                                @error('description')
                                <span class="invalid-feedback-custom">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="custom-file-container" data-upload-id="myFirstImage">
                                    <label>Upload Image (Single File)
                                        <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                           title="Clear Image">x</a>

                                        @error('image') <span style="color: #e7515a">{{$message}}</span> @enderror
                                    </label>
                                    <label class="custom-file-container__custom-file">
                                        <input type="file" name="image"
                                               class="custom-file-container__custom-file__custom-file-input"
                                               accept="image/*">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                    </label>
                                    <div class="custom-file-container__image-preview"></div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">

        $('#textarea, #validationCustom01').maxlength({
            alwaysShow: true,
            placement: "bottom-right",
        });

        new SimpleMDE({
            element: document.getElementById("demo1"),
            spellChecker: false,
        });

        let firstUpload = new FileUploadWithPreview('myFirstImage', {
            images: {
                baseImage: '{{$news->img_url}}',
            },
        })
    </script>
@endsection
