@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Subjects</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">

                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">
                                    <a href="{{url('admin-panel/subjects/create')}}" class="badge outline-badge-info shadow-none">
                                        Create
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($subjects))
                                @foreach ($subjects as $subject)
                                    <tr>
                                        <td>{{$subject->name}}</td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <div class="usr-img-frame mr-2">
                                                    <img alt="avatar" class="img-fluid" src="{{$subject->img_url}}">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center w-25">{{$subject->created_at}}</td>
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <a href="{{url('admin-panel/subjects/'.$subject->id)}}" data-toggle="tooltip"
                                                   data-placement="top"
                                                   class="btn btn-link"
                                                   title="Edit">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                         fill="none"
                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit-2">
                                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                    </svg>
                                                </a>

                                                <a href="{{url('admin-panel/subjects/'.$subject->id.'/items')}}" data-toggle="tooltip"
                                                   data-placement="top"
                                                   class="btn btn-link"
                                                   title="Add Subject Items">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                         stroke-linejoin="round" class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"/>
                                                        <line x1="5" y1="12" x2="19" y2="12"/>
                                                    </svg>
                                                </a>

                                                <form action="{{url('admin-panel/subjects/'.$subject->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Delete"
                                                            class="btn btn-link check-form"
                                                    >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-trash-2">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        $('.check-form').on('click', function (e) {
            if (confirm('Do you really want to delete? ')) {
                this.closest('form').submit();
            }
        })
    </script>
@endsection
