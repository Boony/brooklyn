@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div id="custom_styles" class="col-lg-12 layout-spacing col-md-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Custom styles</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/subjects')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-4 mb-4">
                                <label for="validationCustom01">Name</label>
                                <input type="text" name="name"
                                       class="form-control @error('name') is-invalid @enderror"
                                       id="validationCustom01"
                                       placeholder="Name"
                                       value=""
                                >
                                @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="custom-file-container" data-upload-id="myFirstImage">
                                    <label>Upload Image (Single File)
                                        <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a>

                                        @error('image') <span style="color: #e7515a">{{$message}}</span> @enderror
                                    </label>
                                    <label class="custom-file-container__custom-file">
                                        <input type="file" name="image"
                                               class="custom-file-container__custom-file__custom-file-input"
                                               accept="image/*">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                    </label>
                                    <div class="custom-file-container__image-preview"></div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        let firstUpload = new FileUploadWithPreview('myFirstImage')
    </script>
@endsection
