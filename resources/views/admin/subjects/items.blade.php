@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>


    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$subject->name}}</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <div class="d-flex justify-content-around">
                        <img src="{{$subject->img_url}}" alt="Subject" height="350px">
                    </div>
                </div>

            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Items</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($subject->items))
                                @foreach ($subject->items as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <form action="{{url('admin-panel/subjects/item/'.$item->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Delete"
                                                            class="btn btn-link check-form"
                                                    >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-trash-2">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Create Item</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="">
                        <form class="needs-validation" action="{{'/admin-panel/subjects/'.$subject->id.'/items'}}" method="POST">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-4">
                                    <label for="validationCustom01">Item Name</label>
                                    <input type="text" name="name"
                                           class="form-control @error('name') is-invalid @enderror"
                                           id="validationCustom01"
                                           placeholder="Name"
                                           value=""
                                    >
                                    @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary mt-3" type="submit">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        $('.check-form').on('click', function (e) {
            if (confirm('Do you really want to delete? ')) {
                this.closest('form').submit();
            }
        })
    </script>
@endsection
