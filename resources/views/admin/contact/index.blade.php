@extends('layouts.admin')

@section('content')
    @parent
    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div id="custom_styles" class="col-lg-12 layout-spacing col-md-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>News form</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/contact')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$contact->id}}">
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="phone">Phone</label>
                                <input type="text" name="phone"
                                       class="form-control placement-bottom-right @error('phone') is-invalid @enderror"
                                       id="phone"
                                       placeholder="Phone"
                                       maxlength="14"
                                       value="{{$contact->phone}}"
                                >
                                @error('phone')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="phone">Email</label>
                                <input type="text" name="email"
                                       class="form-control placement-bottom-right @error('email') is-invalid @enderror"
                                       id="email"
                                       placeholder="Email"
                                       maxlength="255"
                                       value="{{$contact->email}}"
                                >
                                @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="facebook">Facebook</label>
                                <input type="text" name="facebook"
                                       class="form-control placement-bottom-right @error('facebook') is-invalid @enderror"
                                       id="facebook"
                                       placeholder="Facebook"
                                       maxlength="255"
                                       value="{{$contact->facebook}}"
                                >
                                @error('facebook')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="instagram">Instagram</label>
                                <input type="text" name="instagram"
                                       class="form-control placement-bottom-right @error('instagram') is-invalid @enderror"
                                       id="instagram"
                                       placeholder="Instagram"
                                       maxlength="255"
                                       value="{{$contact->instagram}}"
                                >
                                @error('instagram')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4">
                                <label for="telegram">Telegram</label>
                                <input type="text" name="telegram"
                                       class="form-control placement-bottom-right @error('telegram') is-invalid @enderror"
                                       id="telegram"
                                       placeholder="Telegram"
                                       maxlength="255"
                                       value="{{$contact->telegram}}"
                                >
                                @error('telegram')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
