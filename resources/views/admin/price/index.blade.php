@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Subjects</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">

                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($subjects))
                                @foreach ($subjects as $item)
                                    <tr class="@if (isset($subject) && $item->id == $subject->id) table-info @endif">
                                        <td>{{$item->name}}</td>
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <a href="{{url('admin-panel/price/'.$item->id)}}" data-toggle="tooltip"
                                                   data-placement="top"
                                                   class="btn btn-link"
                                                   title="Add price list">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                         stroke-linejoin="round" class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"/>
                                                        <line x1="5" y1="12" x2="19" y2="12"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="2" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if (isset($subject))
            <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
                <div class="statbox widget box box-shadow widget-content widget-content-area">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Price list</h4>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Title</th>
                                <th class="text-center">Month</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($subject) && count($subject->prices) > 0 )
                                @foreach ($subject->prices as $item)
                                    <tr class=" @if (isset($price) && $item->id == $price->id) table-info @endif">
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->month}}</td>
                                        <td>{{$item->price}}</td>
                                        <td style="width: 135px">
                                            <div class="d-flex justify-content-around" style="max-width: 135px">
                                                <a href="{{url('admin-panel/price/'.$subject->id.'/item/'.$item->id)}}" data-toggle="tooltip"
                                                   data-placement="top"
                                                   class="btn btn-link"
                                                   title="Edit">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                         fill="none"
                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit-2">
                                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                    </svg>
                                                </a>

                                                <form action="{{url('admin-panel/price/'.$item->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Delete"
                                                            class="btn btn-link check-form"
                                                    >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-trash-2">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        @if (count($subject->items) > 0)

                            <div class="col-md-12">
                                <h4>@if (isset($price)) Update @else Create @endif price</h4>
                            </div>
                            <div class="col-xl-12col-lg-12 col-md-12 col-12 layout-spacing">
                                @if (isset($price))
                                    <form class="needs-validation" method="POST"
                                          action="{{'/admin-panel/price/'.$subject->id.'/item/'.$price->id}}">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-row">
                                            <div class="col-md-4 mb-4">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover mb-4">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Items</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if (isset($subject))
                                                            @foreach ($subject->items as $item)
                                                                <tr>
                                                                    <td>{{$item->name}}</td>
                                                                    <td class="checkbox-column text-center">
                                                                        <div class="custom-control custom-checkbox checkbox-primary">
                                                                            <input name="items[]" value="{{$item->id}}"
                                                                                   type="checkbox" class="custom-control-input todochkbox"
                                                                                   id="todo-{{$item->id}}">
                                                                            <label class="custom-control-label" for="todo-{{$item->id}}"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            @error('items') <span style="color: #e7515a">{{$message}}</span> @enderror
                                                        @else
                                                            <tr>
                                                                <td class="text-center">Empty</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mb-4">
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-4">
                                                        <label for="validationCustom01">Price title</label>
                                                        <input type="text" name="title"
                                                               class="form-control @error('title') is-invalid @enderror"
                                                               id="validationCustom01"
                                                               placeholder="Title"
                                                               value="{{$price->title}}"
                                                        >
                                                        @error('title')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-4 mb-4">
                                                        <label for="validationCustom02">Month</label>
                                                        <input type="number" name="month"
                                                               class="form-control @error('month') is-invalid @enderror"
                                                               id="validationCustom02"
                                                               placeholder="Month"
                                                               value="{{$price->month}}"
                                                        >
                                                        @error('month')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                    <div class="col-md-8 mb-4">
                                                        <label for="validationCustom03">Price</label>
                                                        <input type="number" name="price"
                                                               class="form-control @error('price') is-invalid @enderror"
                                                               id="validationCustom03"
                                                               placeholder="Price"
                                                               value="{{$price->price}}"
                                                        >
                                                        @error('price')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary mt-3" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <form class="needs-validation" action="{{'/admin-panel/price/'.$subject->id}}" method="POST">
                                        @csrf
                                        <div class="form-row">
                                            <div class="col-md-4 mb-4">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover mb-4 table-checkable">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Items</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if (isset($subject))
                                                            @foreach ($subject->items as $item)
                                                                <tr>
                                                                    <td>{{$item->name}}</td>
                                                                    <td class="checkbox-column text-center">
                                                                        <div class="custom-control custom-checkbox checkbox-primary">
                                                                            <input name="items[]" value="{{$item->id}}"
                                                                                   type="checkbox" class="custom-control-input todochkbox"
                                                                                   id="todo-{{$item->id}}">
                                                                            <label class="custom-control-label" for="todo-{{$item->id}}"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan="2" class="text-center">Empty</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mb-4">
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-4">
                                                        <label for="validationCustom01">Price title</label>
                                                        <input type="text" name="title"
                                                               class="form-control @error('title') is-invalid @enderror"
                                                               id="validationCustom01"
                                                               placeholder="Title"
                                                               value=""
                                                        >
                                                        @error('title')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-4 mb-4">
                                                        <label for="validationCustom02">Month</label>
                                                        <input type="number" name="month"
                                                               class="form-control @error('month') is-invalid @enderror"
                                                               id="validationCustom02"
                                                               placeholder="Month"
                                                               value=""
                                                        >
                                                        @error('month')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                    <div class="col-md-8 mb-4">
                                                        <label for="validationCustom03">Price</label>
                                                        <input type="number" name="price"
                                                               class="form-control @error('price') is-invalid @enderror"
                                                               id="validationCustom03"
                                                               placeholder="Price"
                                                               value=""
                                                        >
                                                        @error('price')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary mt-3" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        @elseif(isset($subject))
                            <div class="col-xl-12col-lg-12 col-md-12 col-12 layout-spacing">
                                <div class="statbox widget box box-shadow widget-content widget-content-area">
                                    No subject Items <a href="{{url('/admin-panel/subjects/'.$subject->id.'/items')}}">create item</a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        $('.check-form').on('click', function (e) {
            if (confirm('Do you really want to delete? ')) {
                this.closest('form').submit();
            }
        })
    </script>
@endsection
