@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Create lesson tests</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <form class="needs-validation" action="{{url('admin-panel/test')}}" method="POST">
                        @csrf
                        <input type="hidden" name="lesson_id" value="{{$lesson->id}}">

                        <div class="form-row">
                            <div class="col-md-12 mb-4">
                                <label for="question">Question</label>
                                <textarea id="question" name="question"
                                          class="form-control @error('question') is-invalid @enderror"
                                          rows="5"></textarea>
                                @error('question')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="a">A</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.0') is-invalid @enderror"
                                               id="a"
                                               placeholder="A"
                                               maxlength="255">
                                        @error('test.0')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio1"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio1" name="is_true"
                                                   class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="customRadio1">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="b">B</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.1') is-invalid @enderror"
                                               id="b"
                                               placeholder="B"
                                               maxlength="255">
                                        @error('test.1')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio2"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio2" name="is_true"
                                                   class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="customRadio2">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="c">C</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.2') is-invalid @enderror"
                                               id="c"
                                               placeholder="C"
                                               maxlength="255">
                                        @error('test.2')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio3"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio3" name="is_true"
                                                   class="custom-control-input" value="2">
                                            <label class="custom-control-label" for="customRadio3">Is true</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-row">
                                    <div class="col-md-10 mb-4">
                                        <label for="d">D</label>
                                        <input type="text" name="test[]"
                                               class="form-control placement-bottom-right @error('test.3') is-invalid @enderror"
                                               id="d"
                                               placeholder="D"
                                               maxlength="255">
                                        @error('test.3')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 mb-4">
                                        <label for="customRadio4"></label>
                                        <div class="custom-control custom-radio" style="padding-top: 20px;">
                                            <input type="radio" id="customRadio4" name="is_true"
                                                   class="custom-control-input @error('is_true') is-invalid @enderror"
                                                   value="3">
                                            <label class="custom-control-label" for="customRadio4">Is true</label>
                                            @error('is_true')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Lesson tests</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Question</th>
                                <th class="text-center">A</th>
                                <th class="text-center">B</th>
                                <th class="text-center">C</th>
                                <th class="text-center">D</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($tests))
                                @foreach ($tests as $item)
                                    <tr class="">
                                        <td>{{$item->question}}</td>
                                        @foreach($item->answers as $answer)
                                            <td class="{{ $answer->is_true ? 'btn-success' : '' }}">{{$answer->answer}} </td>
                                        @endforeach
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <form action="{{url('admin-panel/test/'.$item->id)}}"
                                                      method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" data-toggle="tooltip" data-placement="top"
                                                            title="Delete"
                                                            class="btn btn-link check-form"
                                                    >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                             height="24" viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2"
                                                             stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-trash-2">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        $('.check-form').on('click', function (e) {
            if (confirm('Do you really want to delete? ')) {
                this.closest('form').submit();
            }
        })
    </script>
@endsection
