@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Create video lessons</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{url('admin-panel/lesson')}}" method="POST" enctype="multipart/form-data"
              style="display: contents">
            @csrf
            <input type="hidden" name="level_id" value="{{$level->id}}">
            <input type="hidden" name="subject_id" value="{{$subject->id}}">
            <div class="col-12">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-content widget-content-area">
                                <div class="">
                                    <label for="lesson">Lesson</label>
                                    <input type="number" name="lesson"
                                           class="form-control @error('lesson') is-invalid @enderror"
                                           id="lesson" placeholder="Lesson">
                                    @error('lesson')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Uzbek</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                                <div class="form-group mb-4">
                                    <label for="title_uz">Title</label>
                                    <input type="text" name="title_uz"
                                           class="form-control @error('title_uz') is-invalid @enderror"
                                           id="title_uz" placeholder="Title Uz">
                                    @error('title_uz')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-4">
                                    <label for="textarea">Description Uz</label>
                                    <textarea id="textarea" name="description_uz"
                                              class="form-control @error('description_uz') is-invalid @enderror"
                                              maxlength="500" rows="5"></textarea>
                                    @error('description_uz')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-4">
                                    <div class="custom-file-container" data-upload-id="videoUz">
                                        <label>Video Uz (Single File)
                                            <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                               title="Clear Video">x</a>

                                            @error('video_uz') <span
                                                style="color: #e7515a">{{$message}}</span> @enderror
                                        </label>
                                        <label class="custom-file-container__custom-file">
                                            <input type="file" name="video_uz"
                                                   class="custom-file-container__custom-file__custom-file-input"
                                                   accept="video/*">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                            <span
                                                class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="custom-file-container" data-upload-id="bookUz">
                                        <label>Book Uz (Single File)
                                            <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                               title="Upload book">x</a>

                                            @error('book_uz') <span style="color: #e7515a">{{$message}}</span> @enderror
                                        </label>
                                        <label class="custom-file-container__custom-file">
                                            <input type="file" name="book_uz"
                                                   class="custom-file-container__custom-file__custom-file-input">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                            <span
                                                class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="homework">Homework Uz</label>
                                    <textarea id="homework" name="homework_uz"
                                              class="form-control @error('homework_uz') is-invalid @enderror"
                                              maxlength="500" rows="5"></textarea>
                                    @error('homework_uz')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Rus</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                                <div class="form-group mb-4">
                                    <label for="title_ru">Title</label>
                                    <input type="text" name="title_ru"
                                           class="form-control @error('title_ru') is-invalid @enderror"
                                           id="title_ru" placeholder="Title Ru">
                                    @error('title_ru')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-4">
                                    <label for="textarea">Description Ru</label>
                                    <textarea id="textarea1" name="description_ru"
                                              class="form-control @error('description_ru') is-invalid @enderror"
                                              maxlength="500" rows="5"></textarea>
                                    @error('description_ru')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-4">
                                    <div class="custom-file-container" data-upload-id="videoRu">
                                        <label>Video Ru (Single File)
                                            <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                               title="Clear Video">x</a>

                                            @error('video_ru') <span
                                                style="color: #e7515a">{{$message}}</span> @enderror
                                        </label>
                                        <label class="custom-file-container__custom-file">
                                            <input type="file" name="video_ru"
                                                   class="custom-file-container__custom-file__custom-file-input"
                                                   accept="video/*">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                            <span
                                                class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="custom-file-container" data-upload-id="bookRu">
                                        <label>Book Ru (Single File)
                                            <a href="javascript:void(0)" class="custom-file-container__image-clear"
                                               title="Upload book">x</a>

                                            @error('book_ru') <span style="color: #e7515a">{{$message}}</span> @enderror
                                        </label>
                                        <label class="custom-file-container__custom-file">
                                            <input type="file" name="book_ru"
                                                   class="custom-file-container__custom-file__custom-file-input">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                            <span
                                                class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="homework1">Homework Ru</label>
                                    <textarea id="homework1" name="homework_ru"
                                              class="form-control @error('homework_ru') is-invalid @enderror"
                                              maxlength="500" rows="5"></textarea>
                                    @error('homework_ru')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header left">
                        <button type="submit" class="btn btn-primary m-2">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('customScript')
    <script type="text/javascript">

        $('#textarea').maxlength({
            alwaysShow: true,
            placement: "bottom-right",
        });
        $('#textarea1').maxlength({
            alwaysShow: true,
            placement: "bottom-right",
        });

        let videoUz = new FileUploadWithPreview('videoUz')
        let videoRu = new FileUploadWithPreview('videoRu')

        let bookUz = new FileUploadWithPreview('bookUz')
        let bookRu = new FileUploadWithPreview('bookRu')
    </script>
@endsection
