@extends('layouts.admin')

@section('content')
    @parent

    <div class="page-header">
        <div class="page-title">

        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Subjects</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">

                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($subjects))
                                @foreach ($subjects as $item)
                                    <tr class="@if (isset($subject) && $item->id == $subject->id) table-info @endif">
                                        <td>{{$item->name}}</td>
                                        <td class="text-center w-25">
                                            <div class="d-flex justify-content-lg-around">
                                                <a href="{{url('admin-panel/lesson/'.$item->id.'/level')}}"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   class="btn btn-link"
                                                   title="Show levels">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24"
                                                         fill="none" stroke="currentColor" stroke-width="2"
                                                         stroke-linecap="round"
                                                         stroke-linejoin="round" class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"/>
                                                        <line x1="5" y1="12" x2="19" y2="12"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="2" class="text-center">Empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if (isset($levels))
            <div class="col-xl-6 col-lg-6 col-md-6 col-6 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Levels</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">

                            <table class="table table-bordered table-hover mb-4">
                                <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($levels))
                                    @foreach ($levels as $item)
                                        <tr class="@if (isset($level) && $item->id == $level->id) table-info @endif">
                                            <td>{{$item->name}}</td>
                                            <td class="text-center w-25">
                                                <div class="d-flex justify-content-lg-around">
                                                    <a href="{{url('admin-panel/lesson/'.$subject->id.'/level/'.$item->id)}}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       class="btn btn-link"
                                                       title="Show lessons">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24"
                                                             fill="none" stroke="currentColor" stroke-width="2"
                                                             stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-plus">
                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                            <line x1="5" y1="12" x2="19" y2="12"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" class="text-center">Empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (isset($lessons))
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Lessons</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover mb-4">
                                <thead>
                                <tr>

                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Level</th>
                                    <th class="text-center">Lesson</th>
                                    <th class="text-center">Title uz</th>
                                    <th class="text-center">Title ru</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($lessons))
                                    @foreach ($lessons as $item)
                                        <tr class="">
                                            <td>{{$item->subject->name}}</td>
                                            <td>{{$item->level->name}}</td>
                                            <td>{{$item->lesson}}</td>
                                            <td>{{$item->title_uz}}</td>
                                            <td>{{$item->title_ru}}</td>
                                            <td class="text-center w-25">
                                                <div class="d-flex justify-content-lg-around">
                                                    <a href="{{url('admin-panel/lesson/'.$subject->id.'/subject/'.$item->id.'/test')}}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       class="btn btn-link"
                                                       title="Add test">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24"
                                                             fill="none" stroke="currentColor" stroke-width="2"
                                                             stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-plus">
                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                            <line x1="5" y1="12" x2="19" y2="12"/>
                                                        </svg>
                                                    </a>

                                                    <a href="{{url('admin-panel/lesson/'.$subject->id.'/level/'.$level->id.'/subject/'.$item->id)}}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       class="btn btn-link"
                                                       title="Edit lesson">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24"
                                                             fill="none"
                                                             stroke="currentColor" stroke-width="2"
                                                             stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-edit-2">
                                                            <path
                                                                d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                        </svg>
                                                    </a>
                                                    <form action="{{url('admin-panel/lesson/'.$item->id)}}"
                                                          method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="button" data-toggle="tooltip" data-placement="top"
                                                                title="Delete"
                                                                class="btn btn-link check-form"
                                                        >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24" viewBox="0 0 24 24"
                                                                 fill="none"
                                                                 stroke="currentColor" stroke-width="2"
                                                                 stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-trash-2">
                                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                                <path
                                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                                            </svg>
                                                        </button>
                                                        @csrf
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                        <a href="{{url('admin-panel/lesson/'.$subject->id.'/level/'.$level->id.'/create')}}"
                           class="btn btn-outline-info">Create</a>

                    </div>
                </div>
            </div>
        @endif

    </div>
@endsection

@section('customScript')
    <script type="text/javascript">
        $('.check-form').on('click', function (e) {
            if (confirm('Do you really want to delete? ')) {
                this.closest('form').submit();
            }
        })
    </script>
@endsection
