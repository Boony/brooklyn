(function($) {
    $(function() {
        $('nav ul li > a:not(:only-child)').click(function(e) {
            $(this).siblings('.nav-dropdown').toggle();
            $('.nav-dropdown').not($(this).siblings()).hide();
            e.stopPropagation();
        });
        $('html').click(function() {
            $('.nav-dropdown').hide();
        });
        $('#nav-toggle').on('click', function() {
            this.classList.toggle('active');
        });
        $('#nav-toggle').click(function() {
            $('nav ul').toggle();
        });
    });
})(jQuery);



$(".lazy").slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true,
    autoplay: true,
    autoplaySpeed: 1000
});


function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

// wrap the input in a new div that we can style to hide the default checkboxes
$(".option").wrap("<div class='new'></div>");

// add a span after each checkbox that we can style as our new checkboxes
$('.option input[type="checkbox"]').after('<span class="check-box"></span>');

$('.option input[type="radio"]').after('<span class="check-box"></span>');



window.sr = ScrollReveal();
sr.reveal(".fadeUp-animate", {
    origin: "top",
    distance: "100%",
    duration: 1500,
    easing: "ease",
    mobile: true,
    reset: false
});

window.sr = ScrollReveal();
sr.reveal(".fadeLeft-animate", {
    origin: "left",
    distance: "100%",
    duration: 1500,
    easing: "ease",
    mobile: true,
    reset: false
});
window.sr = ScrollReveal();
sr.reveal(".fadeDown-animate", {
    origin: "bottom",
    distance: "100%",
    duration: 1500,
    easing: "ease",
    mobile: true,
    reset: false
});
window.sr = ScrollReveal();
sr.reveal(".fadeRight-animate", {
    origin: "right",
    distance: "100%",
    duration: 1500,
    easing: "ease",
    mobile: true,
    reset: false
});