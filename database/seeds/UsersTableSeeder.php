<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'superAdmin',
                'email' => 'superAdmin@gmail.com',
                'password' => Hash::make('admin12345'),
            ],
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin12345'),
            ],
        ];

        foreach ($items as $item) {
            \App\User::query()->create($item);
        }

    }
}
