<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceSubjectItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_subject_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price_id')->index();
            $table->unsignedBigInteger('subject_item_id')->index();
            $table->timestamps();

            $table->foreign('price_id')->references('id')->on('prices')->onDelete('CASCADE');
            $table->foreign('subject_item_id')->references('id')->on('subject_items');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_subject_items', function (Blueprint $table) {
            $table->dropIndex(['price_id']);
            $table->dropIndex(['subject_item_id']);

            $table->dropForeign(['price_id']); // Drop foreign key 'user_id' from 'posts' table
            $table->dropForeign(['subject_item_id']); // Drop foreign key 'user_id' from 'posts' table
        });

        Schema::dropIfExists('price_subject_items');
    }
}
