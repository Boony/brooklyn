<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_lessons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('level_id');
            $table->smallInteger('lesson');
            $table->string('video_uz', 255);
            $table->string('video_ru', 255);
            $table->string('title_uz', 255);
            $table->string('title_ru', 255);
            $table->longText('description_uz')->nullable();
            $table->longText('description_ru')->nullable();
            $table->longText('homework_uz')->nullable();
            $table->longText('homework_ru')->nullable();
            $table->string('book_uz', 255)->nullable();
            $table->string('book_ru', 255)->nullable();
            $table->timestamps();

            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_lessons');
    }
}
