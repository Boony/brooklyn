const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css').version();

mix.copy('resources/admin/assets', 'public/admin/assets');
mix.copy('resources/admin/bootstrap', 'public/admin/bootstrap');
mix.copy('resources/admin/plugins', 'public/admin/plugins');
mix.copy('resources/client', 'public/client');


