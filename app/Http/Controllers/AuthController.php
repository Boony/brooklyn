<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Get a JWT token via given credentials.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginView(Request $request)
    {
        return view('site.login');
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'username' => 'required|string|max:255|exists:users,email',
            'password' => 'required|string|min:6'
        ]);

        $user = User::query()->where('email', $data['username'])->first();

        if ($user->type === 'admin') {
            return back()->withErrors($data['username'] = ['User not allowed'])->withInput();
        }

        if (Auth::attempt(['email' => $data['username'], 'password' => $data['password']])) {
            return redirect('/');
        }
        return back()->withErrors($data)->withInput();
    }

    public function registerView()
    {
        return view('site.register');
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'username' => 'required|string|max:255|unique:users,email',
            'password' => 'required|string|min:6',
            'password_confirm' => 'required|same:password',
            'name' => 'required|string|max:255',
        ]);
        $data['email'] = $data['username'];
        $data['password'] = \Hash::make($data['password']);

        $user = new User();
        $user->fill($data);
        if ($user->save()) {
            if (Auth::attempt(['email' => $data['username'], 'password' => $data['password_confirm']])) {
                return redirect('/');
            }
        }
        return back()->withErrors($data)->withInput();
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        \auth()->logout();
        return redirect('/');
    }
}
