<?php


namespace App\Http\Controllers;


use App\Models\About;
use App\Models\Contact;
use App\Models\Level;
use App\Models\News;
use App\Models\Order;
use App\Models\PageImage;
use App\Models\Price;
use App\Models\Question;
use App\Models\Subject;
use App\Models\Subscriber;
use App\Models\Test;
use App\Models\TestAnswer;
use App\Models\VideoLesson;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $subjects = Subject::query()->get();
        $image = PageImage::query()->first();
        return view('site.home', ['subjects' => $subjects, 'image' => $image]);
    }

    public function question(Request $request)
    {
        $data = $request->validate([
            'full_name' => 'required|string|max:150',
            'email' => 'required|email|max:150',
            'description' => 'required|string',
        ]);

        $question = new Question();
        $data['status'] = Question::STATUS_PADDING;
        $question->fill($data);
        if ($question->save()) {
            return redirect('/');
        }


        return back()->withErrors($data)->withInput();
    }

    public function tests()
    {
        $tests = Test::query()
            ->whereNull('video_lesson_id')
            ->get();

        return view('site.tests', ['tests' => $tests]);
    }

    public function check(Request $request)
    {
        $data = $request->validate([
            'answer' => 'required|array',
            'answer.*' => 'integer'
        ]);
        $result = 0;
        $error = 0;
        foreach ($data['answer'] as $key => $value) {
            $answer = TestAnswer::query()
                ->where('id', $value)
                ->first();
            if ($answer->is_true == 1) {
                $result++;
            } else {
                $error++;
            }
        }
        if (\Session::has('answer')) {
            \Session::remove('answer');
        }
        \Session::put('answer', 'Your result, true: ' . $result . ', false: ' . $error);
        if ($request->has('url')) {
            return redirect($request->get('url'));
        }
        return redirect('tests');
    }

    public function about()
    {
        $abouts = About::query()->get();
        return view('site.about', ['abouts' => $abouts]);
    }

    public function video()
    {
        $levels = Level::all();
        $lessons = VideoLesson::query()
            ->select(['lesson'])
            ->groupBy(['lesson'])
            ->get();
        $subjects = Subject::all();

        return view('site.video', ['levels' => $levels, 'lessons' => $lessons, 'subjects' => $subjects]);
    }

    public function price()
    {
        $subjects = Subject::query()
            ->with(['prices.items.item'])
            ->get();
        $contact = Contact::query()->first();
        return view('site.price', ['subjects' => $subjects, 'contact' => $contact]);
    }

    public function news()
    {
        $news = News::query()
            ->where('type', 'news')
            ->where('status', News::STATUS_ACTIVE)->get();
        return view('site.news', ['news' => $news]);
    }

    public function newsOne(News $news)
    {
        $news->files;
        return view('site.newsView', ['news' => $news]);
    }

    public function others()
    {
        $news = News::query()
            ->where('type', 'others')
            ->where('status', News::STATUS_ACTIVE)->get();
        return view('site.others', ['news' => $news]);
    }

    public function lessons(Request $request)
    {
        $lessons = VideoLesson::query()
            ->with(['tests.answers'])
            ->when($request->level_id, function (Builder $lessons) use ($request) {
                $lessons->where('level_id', $request->level_id);
            })
            ->when($request->lesson, function (Builder $lessons) use ($request) {
                $lessons->where('lesson', $request->lesson);
            })
            ->when($request->subject_id, function (Builder $lessons) use ($request) {
                $lessons->where('subject_id', $request->subject_id);
            })
            ->first();

        $next = VideoLesson::query()
            ->where('level_id', $lessons->level_id)
            ->where('lesson', $lessons->lesson + 1)
            ->where('subject_id', $lessons->subject_id)
            ->first();
        $prev = VideoLesson::query()
            ->where('level_id', $lessons->level_id)
            ->where('lesson', $lessons->lesson - 1)
            ->where('subject_id', $lessons->subject_id)
            ->first();

        return view('site.lessons', ['lesson' => $lessons, 'next' => $next, 'prev' => $prev]);
    }

    public function lesson(Subject $subject)
    {
        $lesson = VideoLesson::query()
            ->where('subject_id', $subject->id)
            ->first();

        $next = VideoLesson::query()
            ->where('level_id', $lesson->level_id)
            ->where('lesson', $lesson->lesson + 1)
            ->where('subject_id', $lesson->subject_id)
            ->first();
        $prev = VideoLesson::query()
            ->where('level_id', $lesson->level_id)
            ->where('lesson', $lesson->lesson - 1)
            ->where('subject_id', $lesson->subject_id)
            ->first();


        return view('site.lesson', ['lesson' => $lesson, 'next' => $next, 'prev' => $prev]);
    }

    public function sale()
    {
        return view('site.sale');
    }

    public function subscriber(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email|unique:subscribers,email'
        ]);
        $s = new Subscriber();
        $s->fill($data);
        $s->save();
        return redirect('/');
    }

    public function profile()
    {
        $user = auth()->user();
        $user->makeHidden(['created_at', 'updated_at', 'email_verified_at', 'type', 'email']);

        return view('site.profile', ['user' => $user]);
    }

    public function payment(Price $price)
    {
        $user = auth()->user();

        $order = Order::query()
            ->updateOrCreate(
                ['user_id' => $user->id, 'price_id' => $price->id, 'state' => 1],
                [
                    'user_id' => $user->id,
                    'amount' => $price->price,
                    'state' => Order::STATE_WAITING_PAY,
                    'phone' => $user->email,
                    'price_id' => $price->id
                ]);


        if ($order) {
            return view('site.payment', ['price' => $price, 'user' => $user, 'order' => $order]);
        }
        return back()->withErrors(['order' => 'Order not saved']);
    }
}
