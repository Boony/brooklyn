<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Question;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        return view('admin.questions.index', ['questions' => $questions]);
    }

    public function destroy(Question $question)
    {
        $question->delete();
        return redirect('admin-panel/questions');
    }
}
