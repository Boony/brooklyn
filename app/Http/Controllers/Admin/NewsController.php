<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $news = News::search($request)->where('type', 'news')->paginate();
        return view('admin.news.index', ['news' => $news]);
    }

    public function create()
    {
        $type = 'news';
        return view('admin.news.create', ['type' => $type]);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'title' => 'required|string|max:255',
                'short' => 'required|string|max:255',
                'description' => 'required|string',
                'image' => 'required|image',
                'type' => 'required|string'
            ]);
            $data['status'] = News::STATUS_ACTIVE;
            $data['img'] = $request->file('image')->store('news', 'public');
            $news = new News();
            $news->fill($data);
            $news->save();
            if ($news->type === 'news') {
                return redirect('admin-panel/news');
            }
            return redirect('admin-panel/useful');
        } catch (\ErrorException $e) {
            return redirect('admin-panel/news/create')->withErrors($e)->withInput();
        }
    }

    public function edit(News $news)
    {
        return view('admin.news.update', ['news' => $news]);
    }

    public function update(Request $request, News $news)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255',
            'short' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image',
        ]);
        if ($request->hasFile('image')) {
            if ($news->img && \Storage::disk('public')->exists($news->img)) {
                \Storage::disk('public')->delete($news->img);
            }
            $data['img'] = $request->file('image')->store('news', 'public');
        }
        if ($news->update($data)) {
            if ($news->type === 'news') {
                return redirect('admin-panel/news');
            }
            return redirect('admin-panel/useful');
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(News $news)
    {
        if ($news->img && \Storage::disk('public')->exists($news->img)) {
            \Storage::disk('public')->delete($news->img);
        }
        $news->delete();
        if ($news->type === 'news') {
            return redirect('admin-panel/news');
        }
        return redirect('admin-panel/useful');
    }

    public function change(News $news)
    {
        $news->update(['status' => $news->status == News::STATUS_ACTIVE ? News::STATUS_INACTIVE : News::STATUS_ACTIVE]);
        return $news->type === 'news' ? redirect('admin-panel/news') : redirect('admin-panel/useful');
    }
}
