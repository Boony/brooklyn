<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        try {
            $validator = $request->validate([
                $this->username() => 'required|string|exists:users,email',
                'password' => 'required|string',
            ]);
            $user = User::query()->where('email', $validator['email'])->first();
            if ($user->type === 'user') {
                return back()->withErrors($validator['email'] = ['User not allowed'])->withInput();
            }
            $credentials = $request->only($this->username(), 'password');
            if (\Auth::attempt($credentials)) {
                return redirect()->intended('admin-panel');
            }
        } catch (\ErrorException $e) {
            return redirect('admin-panel/login')
                ->withErrors($e)
                ->withInput();
        }
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function logout()
    {
        auth()->logout();
        return redirect('admin-panel/login');
    }
}
