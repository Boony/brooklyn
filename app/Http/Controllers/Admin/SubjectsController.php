<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Models\SubjectItem;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();
        return view('admin.subjects.index', ['subjects' => $subjects]);
    }

    public function create()
    {
        return view('admin.subjects.create');
    }

    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'name' => 'required|string',
                'image' => 'required|image',
            ]);
            $data['img'] = $request->file('image')->store('subjects', 'public');

            $subject = new Subject();
            $subject->fill($data);
            $subject->save();
            return redirect('admin-panel/subjects');
        } catch (\ErrorException $e) {
            return redirect('admin-panel/subjects/create')->withErrors($e)->withInput();
        }

    }

    public function edit(Subject $subject)
    {
        return view('admin.subjects.update', ['subject' => $subject]);
    }

    public function update(Request $request, Subject $subject)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'image' => 'nullable|image'
        ]);

        if ($request->hasFile('image')) {
            if ($subject->img && \Storage::disk('public')->exists($subject->img)) {
                \Storage::disk('public')->delete($subject->img);
            }
            $data['img'] = $request->file('image')->store('subjects', 'public');
        }

        if ($subject->update($data)) {
            return redirect('admin-panel/subjects');
        } else {
            return back()->withErrors($data)->withInput();
        }
    }

    public function destroy(Subject $subject)
    {
        if ($subject->img && \Storage::disk('public')->exists($subject->img)) {
            \Storage::disk('public')->delete($subject->img);
        }
        $subject->delete();
        return redirect('admin-panel/subjects');
    }

    public function items(Subject $subject)
    {
        $subject->items;
        return view('admin.subjects.items', ['subject' => $subject]);
    }

    public function storeItem(Request $request, Subject $subject)
    {
        $data = $request->validate([
            'name' => 'required|max:255|string'
        ]);
        $data['subject_id'] = $subject->id;
        $item = new SubjectItem();
        $item->fill($data);
        if ($item->save()) {
            return redirect('admin-panel/subjects/' . $subject->id . '/items');
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroyItem(SubjectItem $item)
    {
        $item->subject;
        $item->delete();
        return redirect('admin-panel/subjects/' . $item->subject->id . '/items');
    }
}
