<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contact = Contact::query()->firstOrNew([]);
        return view('admin.contact.index', ['contact' => $contact]);
    }

    public function createOrUpdate(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required',
            'email' => 'nullable|email',
            'facebook' => 'nullable',
            'instagram' => 'nullable',
            'telegram' => 'nullable',
            'id' => 'nullable',
        ]);

        $contact = Contact::query()->updateOrCreate(['id' => $data['id']], $data);
        if ($contact) {
            return redirect('admin-panel/contact');
        }
        return back()->withErrors($data)->withInput($data);
    }
}
