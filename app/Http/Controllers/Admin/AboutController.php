<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $abouts = About::all();
        return view('admin.about.index', ['abouts' => $abouts]);
    }

    public function create()
    {
        return view('admin.about.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'required|image',
            'image_mobil' => 'required|image'
        ]);
        $data['img'] = $request->file('image')->store('about', 'public');
        $data['img_mobil'] = $request->file('image_mobil')->store('about', 'public');
        $about = new About();
        $about->fill($data);
        if ($about->save()) {
            return redirect('admin-panel/about');
        } else {
            return back()->withErrors($data)->withInput();
        }
    }

    public function edit(About $about)
    {
        return view('admin.about.update', ['about' => $about]);
    }

    public function update(Request $request, About $about)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image',
            'image_mobil' => 'nullable|image'
        ]);
        if ($request->hasFile('image')) {
            if ($about->img && \Storage::disk('public')->exists($about->img)) {
                \Storage::disk('public')->delete($about->img);
            }
            $data['img'] = $request->file('image')->store('about', 'public');
        }
        if ($request->hasFile('image_mobil')) {
            if ($about->img_mobil && \Storage::disk('public')->exists($about->img_mobil)) {
                \Storage::disk('public')->delete($about->img_mobil);
            }
            $data['img_mobil'] = $request->file('image_mobil')->store('about', 'public');
        }
        if ($about->update($data)) {
            return redirect('admin-panel/about');
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(About $about)
    {
        if ($about->img_mobil && \Storage::disk('public')->exists($about->img_mobil)) {
            \Storage::disk('public')->delete($about->img_mobil);
        }
        if ($about->img && \Storage::disk('public')->exists($about->img)) {
            \Storage::disk('public')->delete($about->img);
        }

        $about->delete();
        return redirect('admin-panel/about');
    }

}
