<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Models\TestAnswer;
use Illuminate\Http\Request;

class MainTestController extends Controller
{
    public function index()
    {
        $tests = Test::query()->whereNull('video_lesson_id')
            ->get();
        return view('admin.test.index', ['tests' => $tests]);
    }

    public function create()
    {
        return view('admin.test.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'question' => 'required|string',
            'is_true' => 'required',
            'test' => 'required|array',
            'test.*' => 'required|string|max:255'
        ]);

        $test = new Test();
        $test->fill([
            'question' => $data['question'],
        ]);
        if ($test->save()) {
            foreach ($data['test'] as $key => $item) {
                $answer = new TestAnswer();
                $answer->fill([
                    'test_id' => $test->id,
                    'answer' => $item,
                    'is_true' => $key == $data['is_true']
                ]);
                $answer->save();
            }
            return redirect('admin-panel/main-test');
        }
        return back()->withErrors($data)->withInput();
    }

    public function view(Test $test)
    {
        return view('admin.test.update', ['test' => $test]);
    }

    public function update(Request $request, Test $test)
    {
        $data = $request->validate([
            'question' => 'required|string|max:255'
        ]);

        if ($test->update($data)) {
            return redirect('admin-panel/main-test');
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(Test $test)
    {
        $test->delete();
        return redirect('admin-panel/main-test');
    }
}
