<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Price;
use App\Models\PriceSubjectItem;
use App\Models\Subject;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function index()
    {
        $subjects = Subject::query()->get();
        return view('admin.price.index', ['subjects' => $subjects]);
    }

    public function createSubjectPrice(Subject $subject)
    {
        $subjects = Subject::query()->get();
        $subject->items;
        $subject->prices;
        return view('admin.price.index', ['subject' => $subject, 'subjects' => $subjects]);
    }

    public function storeSubjectPrice(Request $request, Subject $subject)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255',
            'month' => 'required|integer',
            'price' => 'required|integer',
            'items' => 'required|array'
        ]);
        $data['subject_id'] = $subject->id;

        $price = new Price();
        $price->fill($data);
        if ($price->save()) {
            foreach ($data['items'] as $item) {
                $priceItem = new PriceSubjectItem();
                $priceItem->fill([
                    'price_id' => $price->id,
                    'subject_item_id' => $item
                ]);
                $priceItem->save();
            }
            return redirect('/admin-panel/price/' . $subject->id);
        } else {
            return back()->withErrors($data)->withInput();
        }
    }

    public function editSubjectPrice(Subject $subject, Price $price)
    {
        $subjects = Subject::query()->get();
        $subject->items;
        $subject->prices;
        return view('admin.price.index', ['subject' => $subject, 'subjects' => $subjects, 'price' => $price]);
    }

    public function updateSubjectPrice(Request $request, Subject $subject, Price $price)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255',
            'month' => 'required|integer',
            'price' => 'required|integer',
            'items' => 'required|array'
        ]);

        if (count($data['items']) > 0) {
            PriceSubjectItem::query()->where('price_id', $price->id)->delete();
        }
        foreach ($data['items'] as $item) {
            $priceItem = new PriceSubjectItem();
            $priceItem->fill([
                'price_id' => $price->id,
                'subject_item_id' => $item
            ]);
            $priceItem->save();
        }
        if ($price->update($data)) {
            return redirect('/admin-panel/price/' . $subject->id);
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroySubjectPrice(Price $price)
    {
        $price->subject;
        $price->delete();
        return redirect('/admin-panel/price/' . $price->subject->id);
    }
}
