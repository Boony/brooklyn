<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsFile;
use Illuminate\Http\Request;

class UsefulController extends Controller
{
    public function index(Request $request)
    {
        $news = News::search($request)->where('type', 'others')->paginate();
        return view('admin.news.others', ['news' => $news]);
    }

    public function create()
    {
        $type = 'others';
        return view('admin.news.create', ['type' => $type]);
    }

    public function files(News $news)
    {
        $files = NewsFile::query()->where('news_id', $news->id)->get();
        return view('admin.news.file', ['files' => $files, 'news' => $news]);
    }

    public function storeFile(Request $request, News $news)
    {
        $data = $request->validate([
            'image' => 'required|file|mimes:pdf,ppt,docx,doc,avi,mp3,mp4',
            'title' => 'required|string|max:255',
        ]);

        $data['news_id'] = $news->id;
        $data['url'] = $request->file('image')->store('useful', 'public');
        $file = new NewsFile();
        $file->fill($data);
        if ($file->save()) {
            return redirect('admin-panel/useful/file/' . $news->id);
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroyFile(NewsFile $file)
    {
        if ($file->url && \Storage::disk('public')->exists($file->url)) {
            \Storage::disk('public')->delete($file->url);
        }
        $file->delete();
        return redirect('admin-panel/useful/file/' . $file->news_id);
    }
}
