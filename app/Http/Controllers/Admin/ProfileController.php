<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return view('admin.profile.index', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $data = $request->validate([
            'id' => 'required|integer|exists:users,id',
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users,email,' . $user->id,
            'password' => 'nullable|string|max:255|min:8',
            'password_confirm' => 'nullable|string|max:255|required_with:password|same:password',
        ]);
        if ($data['password']) {
            $data['password'] = \Hash::make($data['password']);
        } else {
            $data['password'] = $user->password;
        }
        $item = User::query()->where('id', $user->id)->first();
        $item->update($data);
        return redirect('admin-panel/profile');
    }
}
