<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Subscriber;

class SubscribersController extends Controller
{
    public function index()
    {
        $subscribers = Subscriber::all();
        return view('admin.subscriber.index', ['subscribers' => $subscribers]);
    }

    public function destroy(Subscriber $subscriber)
    {
        $subscriber->delete();
        return redirect('admin-panel/subscriber');
    }
}
