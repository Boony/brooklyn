<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function index()
    {
        $levels = Level::all();

        return view('admin.level.index', ['levels' => $levels]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);
        $level = new Level();
        $level->fill($data);
        if ($level->save()) {
            return redirect('admin-panel/level');
        }
        return back()->withErrors($data)->withInput();
    }

    public function edit(Level $level)
    {
        $levels = Level::all();
        return view('admin.level.index', ['level' => $level, 'levels' => $levels]);
    }

    public function update(Request $request, Level $level)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);
        if ($level->update($data)) {
            return redirect('admin-panel/level');
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(Level $level)
    {
        $level->delete();
        return redirect('admin-panel/level');
    }
}
