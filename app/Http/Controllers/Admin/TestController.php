<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Models\TestAnswer;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'lesson_id' => 'required|integer|exists:video_lessons,id',
            'question' => 'required|string',
            'is_true' => 'required',
            'test' => 'required|array',
            'test.*' => 'required|string|max:255'
        ]);

        $test = new Test();
        $test->fill([
            'video_lesson_id' => $data['lesson_id'],
            'question' => $data['question'],
        ]);
        if ($test->save()) {
            foreach ($data['test'] as $key => $item) {
                $answer = new TestAnswer();
                $answer->fill([
                    'test_id' => $test->id,
                    'answer' => $item,
                    'is_true' => $key == $data['is_true']
                ]);
                $answer->save();
            }
            return back();
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(Test $test)
    {
        $subject = $test->lesson->subject_id;
        $test->answers()->delete();
        $test->delete();
        return redirect('admin-panel/lesson/' . $subject . '/subject/' . $test->video_lesson_id . '/test');
    }
}
