<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Subject;
use App\Models\Test;
use App\Models\VideoLesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function subjects()
    {
        $subjects = Subject::query()->get();
        return view('admin.lesson.index', ['subjects' => $subjects]);
    }

    public function levels(Subject $subject)
    {
        $subjects = Subject::query()->get();
        $levels = Level::query()->get();
        return view('admin.lesson.index', [
            'subjects' => $subjects,
            'subject' => $subject,
            'levels' => $levels,
        ]);
    }

    public function level(Subject $subject, Level $level)
    {
        $subjects = Subject::query()->get();
        $levels = Level::query()->get();
        $lessons = VideoLesson::query()->with(['subject', 'level'])
            ->where('level_id', $level->id)
            ->where('subject_id', $subject->id)
            ->orderBy('lesson')
            ->get();

        return view('admin.lesson.index', [
            'subjects' => $subjects,
            'subject' => $subject,
            'levels' => $levels,
            'level' => $level,
            'lessons' => $lessons,
        ]);
    }

    public function createLesson(Subject $subject, Level $level)
    {
        return view('admin.lesson.create', ['subject' => $subject, 'level' => $level]);
    }

    public function editLesson(Subject $subject, Level $level, VideoLesson $lesson)
    {
        return view('admin.lesson.update', ['subject' => $subject, 'level' => $level, 'lesson' => $lesson]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title_uz' => 'required|string',
            'title_ru' => 'required|string',
            'description_uz' => 'required|string',
            'description_ru' => 'required|string',
            'homework_uz' => 'required|string',
            'homework_ru' => 'required|string',
            'video_uz' => 'required|file|mimes:mp4,avi',
            'video_ru' => 'required|file|mimes:mp4,avi',
            'book_uz' => 'required|file|mimes:pdf,ppt,docx,doc',
            'book_ru' => 'required|file|mimes:pdf,ppt,docx,doc',
            'level_id' => 'required|exists:levels,id',
            'lesson' => 'required|integer',
            'subject_id' => 'required|exists:subjects,id'
        ]);

        $data['video_uz'] = $request->file('video_uz')->store('video', 'public');
        $data['video_ru'] = $request->file('video_ru')->store('video', 'public');
        $data['book_uz'] = $request->file('book_uz')->store('book', 'public');
        $data['book_ru'] = $request->file('book_ru')->store('book', 'public');

        $lesson = new VideoLesson();
        $lesson->fill($data);
        if ($lesson->save()) {
            return redirect('admin-panel/lesson/' . $lesson->subject_id . '/level/' . $lesson->level_id);
        }
        return back()->withErrors($data)->withInput();
    }

    public function update(Request $request, VideoLesson $lesson)
    {
        $data = $request->validate([
            'title_uz' => 'required|string',
            'title_ru' => 'required|string',
            'description_uz' => 'required|string',
            'description_ru' => 'required|string',
            'homework_uz' => 'required|string',
            'homework_ru' => 'required|string',
            'video_uz' => 'nullable|file|mimes:mp4,avi',
            'video_ru' => 'nullable|file|mimes:mp4,avi',
            'book_uz' => 'nullable|file|mimes:pdf,ppt,docx,doc',
            'book_ru' => 'nullable|file|mimes:pdf,ppt,docx,doc',
            'level_id' => 'required|exists:levels,id',
            'lesson' => 'required|integer',
            'subject_id' => 'required|exists:subjects,id'
        ]);

        if ($request->hasFile('video_uz')) {
            if ($lesson->video_uz && \Storage::disk('public')->exists($lesson->video_uz)) {
                \Storage::disk('public')->delete($lesson->video_uz);
            }
            $data['video_uz'] = $request->file('video_uz')->store('video', 'public');
        }
        if ($request->hasFile('video_ru')) {
            if ($lesson->video_ru && \Storage::disk('public')->exists($lesson->video_ru)) {
                \Storage::disk('public')->delete($lesson->video_ru);
            }
            $data['video_ru'] = $request->file('video_ru')->store('video', 'public');
        }
        if ($request->hasFile('book_uz')) {
            if ($lesson->book_uz && \Storage::disk('public')->exists($lesson->book_uz)) {
                \Storage::disk('public')->delete($lesson->book_uz);
            }
            $data['book_uz'] = $request->file('book_uz')->store('book', 'public');
        }
        if ($request->hasFile('book_ru')) {
            if ($lesson->book_ru && \Storage::disk('public')->exists($lesson->book_ru)) {
                \Storage::disk('public')->delete($lesson->book_ru);
            }
            $data['book_ru'] = $request->file('book_ru')->store('book', 'public');
        }

        if ($lesson->update($data)) {
            return redirect('admin-panel/lesson/' . $lesson->subject_id . '/level/' . $lesson->level_id);
        }
        return back()->withErrors($data)->withInput();
    }

    public function destroy(VideoLesson $lesson)
    {
        if ($lesson->video_uz && \Storage::disk('public')->exists($lesson->video_uz)) {
            \Storage::disk('public')->delete($lesson->video_uz);
        }
        if ($lesson->video_ru && \Storage::disk('public')->exists($lesson->video_ru)) {
            \Storage::disk('public')->delete($lesson->video_ru);
        }
        if ($lesson->book_uz && \Storage::disk('public')->exists($lesson->book_uz)) {
            \Storage::disk('public')->delete($lesson->book_uz);
        }
        if ($lesson->book_ru && \Storage::disk('public')->exists($lesson->book_ru)) {
            \Storage::disk('public')->delete($lesson->book_ru);
        }
        $lesson->delete();
        return redirect('admin-panel/lesson/' . $lesson->subject_id . '/level/' . $lesson->level_id);
    }

    public function tests(Subject $subject, VideoLesson $lesson)
    {
        $tests = Test::query()->with(['answers'])
            ->where('video_lesson_id', $lesson->id)
            ->get();
        return view('admin.lesson.test', ['lesson' => $lesson, 'tests' => $tests]);
    }

}
