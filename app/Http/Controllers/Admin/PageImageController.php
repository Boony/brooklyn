<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\PageImage;
use Illuminate\Http\Request;

class PageImageController extends Controller
{
    public function index()
    {
        $image = PageImage::query()->firstOrNew([]);
//        dd($image->toArray());
        return view('admin.image.index', ['image' => $image]);
    }

    public function createOrUpdate(Request $request)
    {
        $data = $request->validate([
            'id' => 'nullable',
            'home_img' => 'nullable|image'
        ]);

        if ($request->hasFile('home_img')) {
            $data['home_img'] = $request->file('home_img')->store('page', 'public');
        }
        $image = PageImage::query()->updateOrCreate(['id' => $data['id']], $data);
        if ($image) {
            redirect('admin-panel/page-image');
        }
        return back()->withErrors($data)->withInput();
    }
}
