<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 * @property integer $state
 * @property integer $price_id
 * @property string $phone
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends Model
{
    /** Pay in progress, order must not be changed. */
    const STATE_WAITING_PAY = 1;

    /** Order completed and not available for sell. */
    const STATE_PAY_ACCEPTED = 2;

    /** Order is cancelled. */
    const STATE_CANCELLED = 3;


    protected $keyType = 'integer';

    protected $fillable = ['user_id', 'price_id', 'state', 'amount', 'phone'];
}
