<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $subject_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property Subject $subject
 */
class SubjectItem extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    /**
     * @var array
     */

    protected $fillable = [
        'name', 'subject_id', 'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }
}
