<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestAnswer
 * @package App\Models
 * @property integer $id
 * @property integer $test_id
 * @property string $answer
 * @property boolean $is_true
 * @property string $created_at
 * @property string $updated_at
 * @property Test $test
 */
class TestAnswer extends Model
{
    /**
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['test_id', 'answer', 'is_true', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(Test::class, 'id', 'test_id');
    }
}
