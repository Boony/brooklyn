<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $img_mobil
 * @property string $img_mobil_url
 * @property string $img_url
 * @property string $created_at
 * @property string $updated_at
 */
class About extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'img','img_mobil', 'created_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['img_url', 'img_mobil_url'];

    /**
     * @return string|null
     */
    public function getImgUrlAttribute()
    {
        return $this->img ? asset(\Storage::url($this->img)) : null;
    }

    /**
     * @return string|null
     */
    public function getImgMobilUrlAttribute()
    {
        return $this->img_mobil ? asset(\Storage::url($this->img_mobil)) : null;
    }

}
