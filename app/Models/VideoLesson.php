<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $subject_id
 * @property integer $level_id
 * @property integer $lesson
 * @property string $video_uz
 * @property string $video_uz_url
 * @property string $video_ru
 * @property string $video_ru_url
 * @property string $title_uz
 * @property string $title_ru
 * @property string $description_uz
 * @property string $description_ru
 * @property string homework_uz
 * @property string homework_ru
 * @property string $book_uz
 * @property string $book_uz_url
 * @property string $book_ru
 * @property string $book_ru_url
 * @property string $created_at
 * @property string $updated_at
 * @property Test $test
 * @property Test $tests
 */
class VideoLesson extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'subject_id', 'level_id', 'lesson', 'video_uz', 'video_ru', 'title_uz', 'title_ru',
        'description_uz', 'description_ru', 'homework_uz', 'homework_ru', 'book_uz', 'book_ru', 'created_at', 'updated_at'
    ];

    protected $appends = ['video_ru_url', 'video_uz_url', 'book_ru_url', 'book_uz_url'];

    public function getVideoRuUrlAttribute()
    {
        return $this->video_ru ? asset(\Storage::url($this->video_ru)) : null;
    }

    public function getVideoUzUrlAttribute()
    {
        return $this->video_uz ? asset(\Storage::url($this->video_uz)) : null;
    }

    public function getBookRuUrlAttribute()
    {
        return $this->book_ru ? asset(\Storage::url($this->book_ru)) : null;
    }

    public function getBookUzUrlAttribute()
    {
        return $this->book_uz ? asset(\Storage::url($this->book_uz)) : null;
    }

    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    public function level()
    {
        return $this->hasOne(Level::class, 'id', 'level_id');
    }

    public function test()
    {
        return $this->hasOne(Test::class, 'video_lesson_id', 'id');
    }

    public function tests()
    {
        return $this->hasMany(Test::class, 'video_lesson_id', 'id');
    }
}
