<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $img
 * @property string $img_url
 * @property string $created_at
 * @property string $updated_at
 * @property SubjectItem $items
 * @property Price $prices
 */
class Subject extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'img', 'created_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['img_url'];

    /**
     * @return string|null
     */
    public function getImgUrlAttribute()
    {
        return $this->img ? asset(\Storage::url($this->img)) : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(SubjectItem::class, 'subject_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(Price::class, 'subject_id', 'id');
    }

}
