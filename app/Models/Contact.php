<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Contact
 * @package App\Models
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $facebook
 * @property string $instagram
 * @property string $telegram
 * @property string $created_at
 * @property string $updated_at
 */
class Contact extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'phone', 'email', 'facebook', 'instagram', 'telegram', 'created_at', 'updated_at'
    ];

}
