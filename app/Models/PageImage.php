<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class PageImage
 * @package App\Models
 * @property integer $id
 * @property string $home_img
 * @property string $home_img_url
 */
class PageImage extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    /**
     * @var array
     */
    protected $fillable = [
        'home_img', 'created_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['home_img_url'];

    /**
     * @return string|null
     */
    public function getHomeImgUrlAttribute()
    {
        return $this->home_img ? asset(\Storage::url($this->home_img)) : null;
    }
}
