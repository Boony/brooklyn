<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $price_id
 * @property integer $subject_item_id
 * @property string $created_at
 * @property string $updated_at
 * @property SubjectItem $item
 */
class PriceSubjectItem extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'price_id', 'subject_item_id', 'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function item()
    {
        return $this->hasOne(SubjectItem::class, 'id', 'subject_item_id');
    }
}
