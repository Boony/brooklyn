<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $subject_id
 * @property string $title
 * @property integer $month
 * @property integer $price
 * @property string $created_at
 * @property string $updated_at
 * @property Subject $subject
 * @property PriceSubjectItem[] $items
 */
class Price extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'subject_id', 'month', 'price', 'created_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['img_url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(PriceSubjectItem::class, 'price_id', 'id');
    }

}
