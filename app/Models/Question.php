<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $status
 * @property string $full_name
 * @property string $email
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Question extends Model
{
    const STATUS_ACTIVE = 10;
    const STATUS_PADDING = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'description', 'status', 'created_at', 'updated_at'
    ];
}
