<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsFile
 * @package App\Models
 * @property integer $id
 * @property integer $news_id
 * @property string $title
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 */
class NewsFile extends Model
{
    protected $keyType = 'integer';

    protected $fillable = ['news_id', 'title', 'url', 'created_at', 'updated_at'];

    protected $appends = ['link'];

    public function getLinkAttribute()
    {
        return $this->url ? asset(\Storage::url($this->url)) : null;
    }
}
