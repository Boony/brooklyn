<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscriber
 * @package App\Models
 * @property integer $id
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class Subscriber extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    /**
     * @var array
     */
    protected $fillable = [
        'email', 'created_at', 'updated_at'
    ];

}
