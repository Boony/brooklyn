<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class Subject
 * @package App\Models
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $short
 * @property string $description
 * @property string $type
 * @property string $img
 * @property string $img_url
 * @property string $created_at
 * @property string $updated_at
 * @property NewsFile[] $files
 */
class News extends Model
{
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'short', 'description', 'status', 'img', 'type', 'created_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['img_url'];

    /**
     * @return string|null
     */
    public function getImgUrlAttribute()
    {
        return $this->img ? asset(\Storage::url($this->img)) : null;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function search(Request $request)
    {
        return self::query()
            ->when($request->title, function (Builder $query) use ($request) {
                $query->where('title', 'like', $request->title);
            })
            ->when($request->status, function (Builder $query) use ($request) {
                $query->where('status', $request->status);
            })
            ->orderByDesc('id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(NewsFile::class, 'news_id', 'id');
    }
}
