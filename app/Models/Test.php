<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Test
 * @package App\Models
 * @property integer $id
 * @property integer $video_lesson_id
 * @property string $question
 * @property string $created_at
 * @property string $updated_at
 * @property TestAnswer $answers
 * @property VideoLesson $lesson
 */
class Test extends Model
{
    /**
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $fillable = ['video_lesson_id', 'question', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(TestAnswer::class, 'test_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lesson()
    {
        return $this->hasOne(VideoLesson::class, 'id', 'video_lesson_id');
    }

}
