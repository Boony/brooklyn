<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Subject;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('layouts.admin', function ($view) {
            $action = app('request')->route()->getAction();

            $controller = class_basename($action['controller']);

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action'));
        });

        app('view')->composer('layouts.app', function ($view) {
            $contact = Contact::query()->firstOrNew([]);
            $subjects = Subject::query()->get();
            $view->with(compact(['contact','subjects']));
        });
    }
}
