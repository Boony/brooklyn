<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('/about', 'SiteController@about');
Route::get('/tests', 'SiteController@tests');
Route::post('/check', 'SiteController@check');
Route::get('/price', 'SiteController@price');
Route::get('/video', 'SiteController@video');
Route::get('/news', 'SiteController@news');
Route::get('/news/{news}', 'SiteController@newsOne');
Route::get('/others', 'SiteController@others');
Route::get('/lessons', 'SiteController@lessons');
Route::get('/lesson/{subject}', 'SiteController@lesson');
Route::get('/sale', 'SiteController@sale');
Route::post('site/question', 'SiteController@question');
Route::post('subscriber', 'SiteController@subscriber');

Route::get('/login', 'AuthController@loginView');
Route::post('/login', 'AuthController@login');

Route::get('/register', 'AuthController@registerView');
Route::post('/register', 'AuthController@register');


Route::group(['middleware' => 'auth.basic'], function () {
    Route::post('/logout', 'AuthController@logout');
    Route::get('/site/payment/{price}', 'SiteController@payment');
});
Route::group(['prefix' => 'profile', 'middleware' => 'auth.basic'], function () {
    Route::get('', 'SiteController@profile');
});

Route::group(['prefix' => 'admin-panel'], __DIR__ . '/admin.php');
