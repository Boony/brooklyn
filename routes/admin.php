<?php
Route::get('login', 'Admin\AuthController@showLoginForm')->name('login');
Route::post('login', 'Admin\AuthController@login');
Route::group(['middleware' => 'auth'], function () {
    Route::post('logout', 'Admin\AuthController@logout')->name('logout');
    Route::get('/', 'Admin\DashboardController@index');
    Route::group(['prefix' => 'subjects'], function () {
        Route::get('/{subject}/items', 'Admin\SubjectsController@items');
        Route::post('/{subject}/items', 'Admin\SubjectsController@storeItem');
        Route::delete('/item/{item}', 'Admin\SubjectsController@destroyItem');

        Route::get('/', 'Admin\SubjectsController@index');
        Route::get('/create', 'Admin\SubjectsController@create');
        Route::get('/{subject}', 'Admin\SubjectsController@edit');
        Route::post('/', 'Admin\SubjectsController@store');
        Route::put('/{subject}', 'Admin\SubjectsController@update');
        Route::delete('/{subject}', 'Admin\SubjectsController@destroy');
    });
    Route::group(['prefix' => 'news'], function () {
        Route::get('/change/{news}', 'Admin\NewsController@change');

        Route::get('/', 'Admin\NewsController@index');
        Route::get('/create', 'Admin\NewsController@create');
        Route::get('/{news}', 'Admin\NewsController@edit');
        Route::post('/', 'Admin\NewsController@store');
        Route::put('/{news}', 'Admin\NewsController@update');
        Route::delete('/{news}', 'Admin\NewsController@destroy');
    });
    Route::group(['prefix' => 'useful'], function () {
        Route::get('/file/{news}', 'Admin\UsefulController@files');
        Route::post('/file/{news}', 'Admin\UsefulController@storeFile');
        Route::delete('/file/{file}', 'Admin\UsefulController@destroyFile');

        Route::get('/', 'Admin\UsefulController@index');
        Route::get('/create', 'Admin\UsefulController@create');
    });


    Route::group(['prefix' => 'about'], function () {

        Route::get('/', 'Admin\AboutController@index');
        Route::get('/create', 'Admin\AboutController@create');
        Route::get('/{about}', 'Admin\AboutController@edit');
        Route::post('/', 'Admin\AboutController@store');
        Route::put('/{about}', 'Admin\AboutController@update');
        Route::delete('/{about}', 'Admin\AboutController@destroy');
    });
    Route::group(['prefix' => 'price'], function () {
        Route::get('/{subject}/item/{price}', 'Admin\PriceController@editSubjectPrice');
        Route::put('/{subject}/item/{price}', 'Admin\PriceController@updateSubjectPrice');

        Route::get('/', 'Admin\PriceController@index');
        Route::get('/{subject}', 'Admin\PriceController@createSubjectPrice');
        Route::post('/{subject}', 'Admin\PriceController@storeSubjectPrice');
        Route::delete('/{price}', 'Admin\PriceController@destroySubjectPrice');
    });
    Route::group(['prefix' => 'lesson'], function () {
        Route::get('/', 'Admin\LessonController@subjects');
        Route::get('/{subject}/level', 'Admin\LessonController@levels');
        Route::get('/{subject}/level/{level}', 'Admin\LessonController@level');
        Route::get('/{subject}/level/{level}/create', 'Admin\LessonController@createLesson');
        Route::get('/{subject}/level/{level}/subject/{lesson}', 'Admin\LessonController@editLesson');
        Route::get('/{subject}/subject/{lesson}/test', 'Admin\LessonController@tests');

        Route::post('/', 'Admin\LessonController@store');
        Route::put('/{lesson}', 'Admin\LessonController@update');
        Route::delete('/{lesson}', 'Admin\LessonController@destroy');
    });
    Route::group(['prefix' => 'test'], function () {
        Route::post('', 'Admin\TestController@store');
        Route::delete('/{test}', 'Admin\TestController@destroy');
    });

    Route::group(['prefix' => 'main-test'], function () {
        Route::get('', 'Admin\MainTestController@index');
        Route::get('/create', 'Admin\MainTestController@create');
        Route::get('/{test}', 'Admin\MainTestController@view');
        Route::post('', 'Admin\MainTestController@store');
        Route::put('/{test}', 'Admin\MainTestController@update');
        Route::delete('/{test}', 'Admin\MainTestController@destroy');
    });


    Route::group(['prefix' => 'level'], function () {
        Route::get('/', 'Admin\LevelController@index');
        Route::get('/{level}', 'Admin\LevelController@edit');
        Route::post('/', 'Admin\LevelController@store');
        Route::put('/{level}', 'Admin\LevelController@update');
        Route::delete('/{level}', 'Admin\LevelController@destroy');
    });
    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', 'Admin\ContactController@index');
        Route::post('/', 'Admin\ContactController@createOrUpdate');
    });
    Route::group(['prefix' => 'page-image'], function () {
        Route::get('/', 'Admin\PageImageController@index');
        Route::post('/', 'Admin\PageImageController@createOrUpdate');
    });
    Route::group(['prefix' => 'subscriber'], function () {
        Route::get('/', 'Admin\SubscribersController@index');
        Route::delete('/{subscriber}', 'Admin\SubscribersController@destroy');
    });
    Route::group(['prefix' => 'questions'], function () {
        Route::get('/', 'Admin\QuestionsController@index');
        Route::delete('/{question}', 'Admin\QuestionsController@destroy');
    });
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'Admin\ProfileController@index');
        Route::post('/', 'Admin\ProfileController@update');
    });
});

Route::get('storage', function () {
    Artisan::call('storage:link');
    echo 'linked';
});
Route::get('migrate', function () {
    Artisan::call('migrate:fresh');
    echo 'migrated';
});
Route::get('seed', function () {
    Artisan::call('db:seed');
    echo 'seed';
});
Route::get('clear', function () {
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('cache:clear');
    Artisan::call('optimize:clear');
    echo 'clear';
});
